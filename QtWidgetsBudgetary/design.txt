--Create New Bank Link
	Open webview page with html/javascript in qt - COMPLETE
	run the javascript - COMPLETE
	once the link is created, the javascript should run to get the public_access key from the server -COMPLETE
	javscript function will return value
	after this is all complete, then get the latest public access key from the server - load from the DB to QT

--Function in python that checks linked bank accounts and checks the connection -> add to db

-in qt run javascript functions that:
	update transactions
	update balances

-in qt get latest transactions and account balances from the server DB
	store in data

-in qt write the ui to pull from the data



--creating new link
--access key sent to db with special just created flag
write python code that requests just created account
gets that just created acccount and sends to C++
then user adds the name for the account

-STILL TO DO:

-edit transaction,

--Server Functionality:
	Update all accounts every day (COMPLETE)

-add create rule functionality (priority) (COMPLETE)

Budgets:
	Add Budget (priority) (COMPLETE)
	Edit Budget (priority) (COMPLETE)

	Display Spending by user (priority) - tied to transactions (COMPLETE)

	Refactor transaction math (use lower and upper bound when iterating transactions) (COMPLETE)

	View Transactions by Budget (linked to adding filter by category functionality) (priority) (COMPLETE)

Accounts:
	Add Manual Account (COMPLETE)
	Edit Account (COMPLETE)

Categories:
	Display Categories 
	Edit Categories
	Add Category

Linked Banks
	Edit Linked Banks
	Remove Linked Banks
	Add Linked Bank (COMPLETE)

Transactions:
	-Add Filter By Category Functionality (priority) - do this first (COMPLETE)
	-Add Filter By Ownership (Join, Specific User) (COMPLETE)

