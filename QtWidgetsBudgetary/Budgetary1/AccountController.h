#pragma once

#include <QObject>
#include "LinkedBankData.h"
#include "AccountData.h"
#include "UserData.h"
#include "TransactionData.h"
#include "CategoryData.h"
#include "BudgetData.h"
#include <qmap.h>
#include <map>

class AccountController  : public QObject
{
	Q_OBJECT

public:
	AccountController(QObject *parent);
	~AccountController();

	QMap<int, AccountData*> _accounts;
	QMap<int, LinkedBankData*> _linkedBanks;
	QMap<int, TransactionData*> _transactions;
	std::map<QDate, std::map<int, TransactionData*> > _transByDate;
	QMap<int, UserData*> _users;
	QMap<int, CategoryData*> _categories;
	QMap<int, BudgetData*> _budgets;

	bool isIncomeCat(int catId);
	int GetIncomeCats(QList<int>& list);
	void GetExpenseCats(QList<int>& list);
	bool checkIfExistsInList(int key, QList<int>& list);

	void ClearData();
};


