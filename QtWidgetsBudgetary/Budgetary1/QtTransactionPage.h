#pragma once

#include <QWidget>
#include "ui_QtTransactionPage.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtTransactionPageClass; };
QT_END_NAMESPACE

class QtTransactionPage : public QWidget
{
	Q_OBJECT

public:
	QtTransactionPage(QWidget *parent = nullptr);
	~QtTransactionPage();

	Ui::QtTransactionPageClass* ui;

private:
};
