#pragma once

#include <QWidget>
#include "ui_QtWebView.h"
#include <QtWebView>
#include <QUrl>

QT_BEGIN_NAMESPACE
namespace Ui { class QtWebViewClass; };
QT_END_NAMESPACE

class QtWebView : public QWidget
{
	Q_OBJECT

public:
	QtWebView(QWidget *parent = nullptr);
	~QtWebView();

	QtWebView* _webView;

private:
	Ui::QtWebViewClass *ui;
};
