#pragma once

#include <QObject>
#include "Enums.h"

class BudgetData  : public QObject
{
	Q_OBJECT

public:
	BudgetData(QObject *parent);
	~BudgetData();

	int _ID;
	int _categoryID;
	QString _name;
	Enums::BudgetType _type;
	double _amount;
	int _startingDay;
	bool _isActive;
};
