#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtWidgetsBudgetary.h"
#include "QtAccounts.h"
#include "QtBudget.h"
#include "QtLinkedBanks.h"
#include "QtTransactions.h"
#include "QtListCategories.h"
#include "AccountController.h"
#include <QtNetwork/qnetworkaccessmanager.h>
#include <QtWebEngineWidgets/qwebengineview.h>
#include <QtNetwork/qnetworkreply.h>
#include "QtPickLinkedAccountName.h"
#include "QtEditCategory.h"
#include "QtTransactionsEdit.h"
#include "QtBudgetEdit.h"
#include "QtAccountForm.h"
#include "QtLinkedBankForm.h"
#include "QtLinkedBankAccountForm.h"
#include "QtSplitTransaction.h"
#include "QtFilterCategory.h"
#include "QtCreateTransactionRule.h"
#include "QtChooseAccountTypeCreate.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtWidgetsBudgetaryClass; };
QT_END_NAMESPACE

class QtWidgetsBudgetary : public QMainWindow
{
    Q_OBJECT

public:
    QtWidgetsBudgetary(QWidget *parent = nullptr);
    ~QtWidgetsBudgetary();

    QNetworkAccessManager* _networkManager;
    QWebEngineView* _webView;
    QWebEngineView* _webViewWindow;

    QtAccounts* _accounts;
    QtBudget * _budgets;
    QtLinkedBanks * _linkedBanks;
    QtTransactions* _transactions;
    QtListCategories* _categories;
    QtPickLinkedAccountName *_pickLinkName;
    QtEditCategory* _editCategory;
    QtTransactionsEdit* _editTransactions;
    QtBudgetEdit* _budgetEdit;
    QtAccountForm* _accountForm;
    QtLinkedBankAccountForm* _linkedBankAccountForm;
    QtLinkedBankForm* _linkedBankForm;
    QtSplitTransaction* _splitTransactionForm;
    QtFilterCategory* _filterTransactionCategories;
    QtCreateTransactionRule * _createTransRule;
    QtChooseAccountTypeCreate* _chooseAccountType;


    AccountController * _accountController;

    QWidget* _currentlyShowingWindow;
    QWidget* _prevShowingWindow;

    QList<QWidget*> _widgets;


    void showWindow(QWidget* pointer);

    void SetDebugText(QString data);

    void SendRequest();

    void ReplyErrorOccured(QNetworkReply::NetworkError error);
    void ShowPrevWindow();
    void ReplyCheck();

    void ClearUI();

    short int _updateCounter;
    short int _getDataCounter;

    void UpdateUIData(bool fullRefresh = false);
    void OnDBAPIUpdated();

    Ui::QtWidgetsBudgetaryClass* ui;

signals:
    void CheckForLinksFinished();
    void DBFinishedUpdating();

public slots:
    void actionAccountsClicked();
    void actionTransactionsClicked();
    void actionBudgetsClicked();
    void actionLinkedBanksClicked();
    void actionCategoriesClicked();
    void replyFinished(QNetworkReply* reply);

    void OnDBFinishedUpdating();


private:


};
