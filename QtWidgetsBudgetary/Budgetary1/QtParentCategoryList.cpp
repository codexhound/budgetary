#include "QtParentCategoryList.h"
#include "StaticH.h"

QtParentCategoryList::QtParentCategoryList(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtParentCategoryListClass())
{
	ui->setupUi(this);

	_ID = 0;

	connect(ui->_edit, &QPushButton::pressed, this, &QtParentCategoryList::EditPressed);
}

void QtParentCategoryList::EditPressed()
{
	QtEditCategory* editCat = StaticController::_mainWindow->_editCategory;
	editCat->_ID = _ID;
	editCat->UpdateUI();
	StaticController::_mainWindow->showWindow(editCat);
}

void QtParentCategoryList::UpdateUI()
{
	auto& catMap = StaticController::_accountsController->_categories;

	for (auto i = catMap.begin(); i != catMap.end(); i++)
	{
		if (i.value()->_parentID == _ID)
		{
			QtChildCatList* childCat = new QtChildCatList(this);
			childCat->_ID = i.key();
			childCat->ui->_isMasterCat->setChecked(i.value()->_masterId);
			childCat->ui->_catName->setText(i.value()->_name);
			_childCatWidgets[i.key()] = childCat;
			ui->_childCatHolder->layout()->addWidget(childCat);
		}
	}
}

QtParentCategoryList::~QtParentCategoryList()
{
	delete ui;
}


