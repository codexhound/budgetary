#pragma once

#include <QWidget>
#include "ui_QtLinkedBanks.h"
#include "QtLinkedBank.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtLinkedBanksClass; };
QT_END_NAMESPACE

class QtLinkedBanks : public QWidget
{
	Q_OBJECT

public:
	QtLinkedBanks(QWidget *parent = nullptr);
	~QtLinkedBanks();

	QMap<int, QtLinkedBank*> _linkedBanks;

	Ui::QtLinkedBanksClass* ui;

	void ClearUI();
	void UpdateUI();

public slots:
	void AddBankPressed();

signals:
	void UpdateLinkedBankUI();

private:
	
};
