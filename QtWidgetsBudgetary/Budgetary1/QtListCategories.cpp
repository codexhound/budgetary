#include "QtListCategories.h"
#include "StaticH.h"

QtListCategories::QtListCategories(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtListCategoriesClass())
{
	ui->setupUi(this);

	connect(this, &QtListCategories::UpdateCategoriesUI, this, &QtListCategories::UpdateUI);
	connect(ui->_createCategory, &QPushButton::pressed, this, &QtListCategories::CreateCategoryPressed);
	
}

QtListCategories::~QtListCategories()
{
	delete ui;
}

void QtListCategories::CreateCategoryPressed()
{
	QtEditCategory* editCat = StaticController::_mainWindow->_editCategory;
	editCat->_ID = 0;
	editCat->UpdateUI();
	StaticController::_mainWindow->showWindow(editCat);
}

void QtListCategories::UpdateUI()
{
	auto& catMap = StaticController::_accountsController->_categories;
	QGridLayout* layout = dynamic_cast<QGridLayout*>(ui->scrollAreaWidgetContents->layout());
	int row = 0;
	int col = 0;
	QString categories;
	for (auto i = catMap.begin(); i != catMap.end(); i++)
	{
		categories = categories + QString::number(i.value()->_ID) + ",ParentID: " + QString::number(i.value()->_parentID) + ",";
		if (i.value()->_parentID <= 0 && layout != nullptr)
		{
			
			QtParentCategoryList* parentCat = new QtParentCategoryList(this);
			parentCat->_ID = i.key();
			parentCat->ui->_isMasterCat->setChecked(i.value()->_masterId);
			parentCat->ui->_ParentCategoryName->setText(i.value()->_name);
			parentCat->UpdateUI();
			_categoryList[i.key()] = parentCat;
			
			layout->addWidget(parentCat, row, col);

			col++;
			if (col > 2)
			{
				col = 0;
				row++;
			}
		}
	}
	ui->lineEdit->setText(categories);

}
void QtListCategories::ClearUI()
{
	for (auto i = _categoryList.begin(); i != _categoryList.end(); i++)
	{
		delete i.value();
	}
	_categoryList.clear();
}
