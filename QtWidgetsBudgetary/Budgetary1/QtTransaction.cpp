#include "QtTransaction.h"
#include "StaticH.h"

QtTransaction::QtTransaction(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtTransactionClass())
{
	ui->setupUi(this);

	_ID = 0;

	connect(ui->_edit, &QPushButton::pressed, this, &QtTransaction::EditPressed);
}

QtTransaction::~QtTransaction()
{
	delete ui;
}

void QtTransaction::EditPressed()
{
	auto& tranMap = StaticController::_accountsController->_transactions;
	auto findData = tranMap.find(_ID);
	TransactionData* tData = nullptr;
	if (findData != tranMap.end())
	{
		tData = findData.value();
		QtTransactionsEdit* editTrans = StaticController::_mainWindow->_editTransactions;

		editTrans->ui->_deleteTranBut->setEnabled(true);
		editTrans->ui->_splitTransBut->setEnabled(true);
		editTrans->OnSetupUI();

		editTrans->_transID = _ID;
		editTrans->ui->_accountCombo->setCurrentIndex(editTrans->_accountComboIndexMapR[tData->_accountId]);
		editTrans->ui->_ownerCombo->setCurrentIndex(editTrans->_ownerComboIndexMapR[tData->_ownership]);
		editTrans->ui->_catCombo->setCurrentIndex(editTrans->_catComboIndexMapR[tData->_catId]);
		int positive = 1;
		if (tData->_amount < 0)
		{
			positive = -1;
		}
		editTrans->ui->_amountSpin->setValue(tData->_amount * positive);
		editTrans->ui->debug->setText(QString::number(tData->_amount * positive));
		int type = 0;
		if (tData->_amount <0) //then this is a credit
		{
			type = 1;
		}
		editTrans->ui->_typeCombo->setCurrentIndex(type);
		editTrans->ui->_name->setText(tData->_name);
		editTrans->ui->_merchantName->setText(tData->_externalName);
		editTrans->ui->_dateEdit->setDate(tData->_date);

		StaticController::_mainWindow->showWindow(editTrans);
	}
}
