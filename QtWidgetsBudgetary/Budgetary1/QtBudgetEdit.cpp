#include "QtBudgetEdit.h"
#include "StaticH.h"
#include <qjsonobject.h>

QtBudgetEdit::QtBudgetEdit(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtBudgetEditClass())
{
	_ID = 0;
	ui->setupUi(this);

	connect(ui->_okButton, &QPushButton::pressed, this, &QtBudgetEdit::OkPressed);
	connect(ui->_cancelButton, &QPushButton::pressed, this, &QtBudgetEdit::CancelPressed);
	connect(ui->_delete, &QPushButton::pressed, this, &QtBudgetEdit::DeletePressed);
}

void QtBudgetEdit::DeletePressed()
{
	//delete the transaction
	QJsonObject obj;
	QJsonObject data;
	obj["username"] = "mjbourquin";
	data["ID"] = _ID;
	data["isActive"] = 0;
	data["catID"] = 0;
	data["amount"] = 0;
	data["name"] = "";
	obj["Data"] = data;
	QString url = "http://WIN-Q75VOTO0D0V:80/updateBudget";
	StaticController::Post(url, obj);

	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_budgets);
}

void  QtBudgetEdit::OkPressed()
{
	//post to the server
	int ID = _ID;
	QString name = ui->_nameEdit->text();
	double amount = ui->_amountSpinner->value();
	int catId = _catComboMap[ui->_categorySelect->currentIndex()];

	if (amount > 0)
	{
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = ID;
		data["isActive"] = 1;
		data["catID"] = catId;
		data["amount"] = amount;
		data["name"] = name;
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateBudget";
		StaticController::Post(url, obj);
	}

	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_budgets);
}

void  QtBudgetEdit::CancelPressed()
{
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_budgets);
}

void QtBudgetEdit::ClearUI()
{
	_ID = 0;
	ui->_categorySelect->clear();
	ui->_amountSpinner->setValue(0.0);
	ui->_nameEdit->setText("");
	_catComboMap.clear();
	_catComboMapR.clear();
}

void QtBudgetEdit::UpdateUI()
{
	ClearUI();

	auto& catMap = StaticController::_accountsController->_categories;
	//auto& budgetMap = StaticController::_accountsController->_budgets;


	int index = 0;
	for (auto i = catMap.begin(); i != catMap.end(); i++)
	{
		if (i.value()->_parentID <= 0 && !StaticController::_accountsController->isIncomeCat(i.key()) && i.value()->_isShown)
		{
			//parent

			ui->_categorySelect->addItem(i.value()->_name);
			_catComboMap.insert(index, i.key());
			_catComboMapR.insert(i.key(), index);

			index++;

			for (auto j = catMap.begin(); j != catMap.end(); j++)
			{
				if (j.value()->_parentID == i.key() && !StaticController::_accountsController->isIncomeCat(j.key()) && j.value()->_isShown)
				{
					ui->_categorySelect->addItem(i.value()->_name + "---->" + j.value()->_name);
					_catComboMap.insert(index, j.key());
					_catComboMapR.insert(j.key(), index);

					index++;
				}
			}
		}
	}
}

QtBudgetEdit::~QtBudgetEdit()
{
	delete ui;
}
