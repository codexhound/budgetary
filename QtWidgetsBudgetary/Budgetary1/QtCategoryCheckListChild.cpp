#include "QtCategoryCheckListChild.h"
#include "StaticH.h"

QtCategoryCheckListChild::QtCategoryCheckListChild(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtCategoryCheckListChildClass())
{
	ui->setupUi(this);

	_ID = 0;

	connect(ui->_checkBox, &QCheckBox::stateChanged, this, &QtCategoryCheckListChild::OnCheckBoxChange);
}

void QtCategoryCheckListChild::OnCheckBoxChange()
{
	bool isChecked = ui->_checkBox->isChecked();
	if (!isChecked)
	{
		StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(false);
		StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setChecked(false);
		StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(true);
	}
	else
	{
		if (StaticController::_mainWindow->_filterTransactionCategories->isAllChecked())
		{
			StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(false);
			StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setChecked(true);
			StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(true);
		}
	}
}

QtCategoryCheckListChild::~QtCategoryCheckListChild()
{
	delete ui;
}
