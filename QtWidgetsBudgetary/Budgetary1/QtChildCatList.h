#pragma once

#include <QWidget>
#include "ui_QtChildCatList.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtChildCatListClass; };
QT_END_NAMESPACE

class QtChildCatList : public QWidget
{
	Q_OBJECT

public:
	QtChildCatList(QWidget *parent = nullptr);
	~QtChildCatList();

	int _ID;

	Ui::QtChildCatListClass* ui;

public slots:
	void EditPressed();

private:
	
};
