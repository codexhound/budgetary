#pragma once

#include <QWidget>
#include "ui_QtBudget.h"
#include "QtBudgetList.h"
#include "QtOtherExpensesBudget.h"
#include "QtIncomeBudget.h"
#include "QtHouseholdMember.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtBudgetClass; };
QT_END_NAMESPACE

class QtBudget : public QWidget
{
	Q_OBJECT

public:
	QtBudget(QWidget *parent = nullptr);
	~QtBudget();

	Ui::QtBudgetClass* ui;

	QMap<int, QtBudgetList*> _budgetList;
	QList<QtIncomeBudget*> _incomeLabels;
	QList<QtOtherExpensesBudget*> _otherLabels;
	QList<QtHouseholdMember*> _members;

	void UpdateUI();
	void ClearUI();

	static QList<int>* includedCatIDs(int budgetCat);
	static bool isTransInBudget(int transCat, QList<int> * catIDs);
	static bool CheckIfCatInBudget(int catId);
	static bool ParentCatAlreadyDisplayed(int catId, QList<int>& list);

	int getCurrentMonthSelection();
	QString getCurrentMonthSelectionQ();
	int getCurrentYearSelection();

	QMap<int, QString> _months;
	QMap<int, int> _years;

public slots:
	void NewBudgetPressed();
	void IncomePressed();
	void ApplyPressed();

signals:
	void UpdateBudgetUI();

private:
	
};
