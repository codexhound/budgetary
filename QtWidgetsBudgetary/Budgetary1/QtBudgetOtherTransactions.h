#pragma once

#include <QWidget>
#include "ui_QtBudgetOtherTransactions.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtBudgetOtherTransactionsClass; };
QT_END_NAMESPACE

class QtBudgetOtherTransactions : public QWidget
{
	Q_OBJECT

public:
	QtBudgetOtherTransactions(QWidget *parent = nullptr);
	~QtBudgetOtherTransactions();

	Ui::QtBudgetOtherTransactionsClass* ui;

private:
	
};
