#pragma once

#include <QObject>
#include "AccountData.h"
#include <qmap.h>

class LinkedBankData  : public QObject
{
	Q_OBJECT

public:
	LinkedBankData(QObject *parent);
	~LinkedBankData();

	int _ID;
	QString _name;
	bool _active;
	bool _linked;
	
};
