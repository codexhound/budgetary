#pragma once

#include <QWidget>
#include "ui_QtCategoryCheckListChild.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtCategoryCheckListChildClass; };
QT_END_NAMESPACE

class QtCategoryCheckListChild : public QWidget
{
	Q_OBJECT

public:
	QtCategoryCheckListChild(QWidget *parent = nullptr);
	~QtCategoryCheckListChild();

	Ui::QtCategoryCheckListChildClass* ui;

	int _ID;
public slots:
	void OnCheckBoxChange();

private:
	
};
