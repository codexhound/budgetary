#pragma once

#include <QObject>

class AccountData  : public QObject
{
	Q_OBJECT

public:
	AccountData(QObject *parent);
	~AccountData();

	int _ID;
	bool _active;
	QString _name;
	double _balance;
	int _linkedBankID;
};
