#include "QtBudget.h"
#include "StaticH.h"
#include "QtBudgetEdit.h"
#include "QtHouseholdMember.h"

QtBudget::QtBudget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtBudgetClass())
{
	ui->setupUi(this);

	_months.insert(1, "Jan");
	_months.insert(2, "Feb");
	_months.insert(3, "March");
	_months.insert(4, "April");
	_months.insert(5, "May");
	_months.insert(6, "June");
	_months.insert(7, "July");
	_months.insert(8, "August");
	_months.insert(9, "Sep");
	_months.insert(10, "Oct");
	_months.insert(11, "Nov");
	_months.insert(12, "Dec");

	
	for (auto i = _months.begin(); i != _months.end(); i++)
	{
		ui->_month->addItem(i.value());
	}
	ui->_month->setCurrentIndex(QDate::currentDate().month() - 1);
	auto* qtyear = ui->_year;

	int yearindex = 0;
	for (int year = 1900; year < 2050; year++)
	{
		qtyear->addItem(QString::number(year));
		_years.insert(yearindex, year);
		yearindex++;
	}
	qtyear->setCurrentIndex(QDate::currentDate().year() - 1900);

	connect(this, &QtBudget::UpdateBudgetUI, this, &QtBudget::UpdateUI);
	connect(ui->_newBudget, &QPushButton::pressed, this, &QtBudget::NewBudgetPressed);
	connect(ui->_totalIncomeButton, &QPushButton::pressed, this, &QtBudget::IncomePressed);
	connect(ui->_applyButton, &QPushButton::pressed, this, &QtBudget::ApplyPressed);
}

void QtBudget::ApplyPressed()
{
	UpdateUI();
}

void QtBudget::NewBudgetPressed()
{
	QtBudgetEdit* budgetEdit = StaticController::_mainWindow->_budgetEdit;

	budgetEdit->UpdateUI();

	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_budgetEdit);
}

void QtBudget::IncomePressed()
{
	QList<int> incomeCats;
	StaticController::_accountsController->GetIncomeCats(incomeCats);
	QString string;
	for (int i = 0; i < incomeCats.size(); i++)
	{
		string = string + QString::number(incomeCats.at(i)) + ",";
	}
	ui->lineEdit->setText(string);

	QtFilterCategory* filterCategory = StaticController::_mainWindow->_filterTransactionCategories;
	QtTransactions* transactions = StaticController::_mainWindow->_transactions;

	QDate startDate = QDate(getCurrentYearSelection(), getCurrentMonthSelection(), 1);
	QDate endDate = startDate.addMonths(1);
	transactions->ui->_endDate->setDate(endDate);
	transactions->ui->_startDate->setDate(startDate);
	transactions->ui->__showIgnored->setChecked(true);

	filterCategory->SetCheckedCategories(incomeCats);
	transactions->UpdateUI();

	StaticController::_mainWindow->showWindow(transactions);
}

QtBudget::~QtBudget()
{
	delete ui;
}

void QtBudget::UpdateUI()
{
	StaticController::_mainWindow->SetDebugText("Updating Budgets UI");
	ClearUI();
	AccountController* controller = StaticController::_accountsController;
	QString isShown;
	if (controller != nullptr)
	{
		BudgetData* data = nullptr;
		QtBudgetList* budget = nullptr;
		QMap<int, BudgetData*>::iterator i;
		QMap<int, BudgetData*>& budgets = controller->_budgets;
		QMap<int, QtBudgetList*>::iterator find;

		auto& transData = StaticController::_accountsController->_transactions;
		auto& transDateData = StaticController::_accountsController->_transByDate;
		auto& catData = StaticController::_accountsController->_categories;

		QDate startDate;
		QDate endDate;

		startDate = QDate(getCurrentYearSelection(), getCurrentMonthSelection(), 1);
		endDate = startDate.addMonths(1);

		QString danger = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #FF0350,stop: 0.4999 #FF0020,stop: 0.5 #FF0019,stop: 1 #FF0000 );border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;border: .px solid black;}";
		QString safe = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #78d,stop: 0.4999 #46a,stop: 0.5 #45a,stop: 1 #238 );border-bottom-right-radius: 7px;border-bottom-left-radius: 7px;border: 1px solid black;}";

		for (i = budgets.begin(); i != budgets.end(); i++)
		{
			data = i.value();
			budget = new QtBudgetList(this);
			budget->_ID = i.key();
			budget->ui->_budgetCatName->setText(data->_name);
			budget->ui->_maxAmount->setText("$" + QString::number(data->_amount));

			double budgetAmount = 0;
			
			if (data->_type == Enums::Monthly)
			{
				startDate = QDate(getCurrentYearSelection(), getCurrentMonthSelection(), 1);
				endDate = startDate.addMonths(1);
			}
			else //yearly
			{
				startDate = QDate(getCurrentYearSelection(), 1, 1);
				endDate = startDate.addYears(1);
			}
			auto catfind1 = catData.find(data->_categoryID);
			
			if (catfind1 != catData.end())
			{
				isShown = isShown + ", " + QString::number(catfind1.value()->_isShown);
			}
			//budget->ui->debug->setText(QString::number(budgetAmount));
			//budget->ui->debug->setText(startDate.toString() + ", " + endDate.toString());
			//budget->ui->debug->setText(QString::number(getCurrentYearSelection()) + ", " + QString::number(getCurrentMonthSelection()));

			QList<int> * catIDs = QtBudget::includedCatIDs(data->_categoryID);
			TransactionData* transValue = nullptr;
			for (auto i = transDateData.lower_bound(startDate); i->first <= endDate && i != transDateData.end(); i++)
			{
				for (auto j = i->second.begin(); j != i->second.end(); j++)
				{
					transValue = j->second;
					auto catFind = catData.find(transValue->_catId);
					if (catFind != catData.end())
					{

						bool catShown = catFind.value()->_isShown;
						if (transValue->_date <= endDate && transValue->_date >= startDate && isTransInBudget(transValue->_catId, catIDs) && catShown)
						{
							budgetAmount = budgetAmount + transValue->_amount;
						}
					}
				}
			}

			budget->_startDate = startDate;
			budget->_endDate = endDate;
			budget->_catList = *catIDs;

			budget->ui->_BudgetBar->setMaximum(data->_amount);
			budget->ui->_BudgetBar->setMinimum(0.0);
			budget->ui->_BudgetBar->setStyleSheet(safe); //highlight red, over budget

			
			budgetAmount = budgetAmount * -1; //make inverse
			double barAmount = 0;
			if (budgetAmount <= 0) //if budget amount is less than 0, then we have actually paid into the budget
			{
				barAmount = 0;
			}
			else if (budgetAmount > data->_amount)
			{
				budget->ui->_BudgetBar->setStyleSheet(danger); //highlight red, over budget
				barAmount = data->_amount;
			}
			else
			{
				barAmount = std::abs(budgetAmount);
			}
			budget->ui->_budgetAmount->setText("$" + QString::number(budgetAmount));
			budget->ui->_BudgetBar->setValue(barAmount);
			

			_budgetList.insert(i.key(), budget);
			ui->_central->layout()->addWidget(budget);

			delete catIDs;
		}

		startDate = QDate(getCurrentYearSelection(), getCurrentMonthSelection(), 1);
		endDate = startDate.addMonths(1);

		//show income this month
		//find the income category(s)

		TransactionData* transValue = nullptr;
		double allincomeTotal = 0;
		QList<int> incomeCats;
		int incomeParentCatId = StaticController::_accountsController->GetIncomeCats(incomeCats);
		//auto findParentCat = catData.find(incomeParentCatId);
		QString income;
		for (auto catiter = 0; catiter < incomeCats.size(); catiter++)
		{
			income = income + QString::number(incomeCats.at(catiter)) + ", ";
			auto catFind = catData.find(incomeCats.at(catiter));
			if (catFind != catData.end())
			{
				double incomeTotal = 0;
				for (auto i = transDateData.lower_bound(startDate); i->first <= endDate && i != transDateData.end(); i++)
				{
					for (auto j = i->second.begin(); j != i->second.end(); j++)
					{
						transValue = j->second;
						if (transValue->_date <= endDate && transValue->_date >= startDate && (transValue->_catId == catFind.key()))
						{
							incomeTotal = incomeTotal + transValue->_amount;
						}
					}
				}
				allincomeTotal = allincomeTotal + incomeTotal;
				if (std::abs(incomeTotal) > 0)
				{
					QtIncomeBudget* incomeLabel = new QtIncomeBudget(this);
					incomeLabel->_catId = catFind.key();
					incomeLabel->ui->_catName->setText(catFind.value()->_name);
					incomeLabel->ui->_amount->setText("$" + QString::number(incomeTotal));
					ui->_income->layout()->addWidget(incomeLabel);
					_incomeLabels.append(incomeLabel);
				}
			}
		}
		ui->lineEdit->setText(income);
		ui->_totalIncome->setText("$" + QString::number(allincomeTotal));

		//show other expenses this month not under the budgets
		CategoryData* catTemp = nullptr;
		double otherExpensesTotal = 0;
		//go through parent categories first and display those, then anything else that is left
		for (auto catIter = catData.begin(); catIter != catData.end(); catIter++)
		{
			catTemp = catIter.value();
			if (catTemp->_parentID <= 0)
			{
				if (!CheckIfCatInBudget(catTemp->_ID) && catIter.value()->_isShown) //not in budget, needs to be displayed
				{
					double otherCatExpenses = 0;
					for (auto i = transDateData.lower_bound(startDate); i->first <= endDate && i != transDateData.end(); i++)
					{
						for (auto j = i->second.begin(); j != i->second.end(); j++)
						{
							transValue = j->second;
							if (transValue->_catId == catTemp->_ID)
							{
								otherCatExpenses = otherCatExpenses + transValue->_amount;
							}
						}
					}
					otherExpensesTotal = otherExpensesTotal + otherCatExpenses;

					if (std::abs(otherCatExpenses) > 0)
					{
						QtOtherExpensesBudget* otherExpenses = new QtOtherExpensesBudget(this);
						otherExpenses->ui->_amount->setText("$" + QString::number(otherCatExpenses));
						otherExpenses->ui->_catNamePush->setText(catTemp->_name);
						otherExpenses->_catId = catIter.key();
						ui->_otherExpenses->layout()->addWidget(otherExpenses);
						_otherLabels.append(otherExpenses);
					}
					//display this category total
				}

				
			}
		}

		//display others
		for (auto catIter = catData.begin(); catIter != catData.end(); catIter++)
		{
			catTemp = catIter.value();
			if (catTemp->_parentID > 0)
			{
				if (!CheckIfCatInBudget(catTemp->_ID) && catIter.value()->_isShown) //not in budget, needs to be displayed
				{
					double otherCatExpenses = 0;
					for (auto i = transDateData.lower_bound(startDate); i->first <= endDate && i != transDateData.end(); i++)
					{
						for (auto j = i->second.begin(); j != i->second.end(); j++)
						{
							transValue = j->second;
							if (transValue->_catId == catTemp->_ID)
							{
								otherCatExpenses = otherCatExpenses + transValue->_amount;
							}
						}
					}
					otherExpensesTotal = otherExpensesTotal + otherCatExpenses;

					//display this category total
					//find the parent cat
					auto parentFind = catData.find(catTemp->_parentID);
					if (parentFind != catData.end() && std::abs(otherCatExpenses) > 0)
					{
						QtOtherExpensesBudget* otherExpenses = new QtOtherExpensesBudget(this);
						otherExpenses->ui->_amount->setText("$" + QString::number(otherCatExpenses));
						otherExpenses->ui->_catNamePush->setText(parentFind.value()->_name + "->" + catTemp->_name);
						otherExpenses->_catId = catIter.key();
						ui->_otherExpenses->layout()->addWidget(otherExpenses);
						_otherLabels.append(otherExpenses);
					}
				}
			}
		}

		ui->_otherExpenseTotal->setText("$" + QString::number(otherExpensesTotal));

		//display user total
		
		double membersTotal = 0;
		double memberTotal = 0;
		auto& userMap = StaticController::_accountsController->_users;
		QList<int> expenseList;
		StaticController::_accountsController->GetExpenseCats(expenseList);
		for (auto i = transDateData.lower_bound(startDate); i->first <= endDate && i != transDateData.end(); i++)
		{
			for (auto j = i->second.begin(); j != i->second.end(); j++)
			{
				auto catFind = catData.find(j->second->_catId);
				if (catFind != catData.end())
				{
					if (StaticController::_accountsController->checkIfExistsInList(j->second->_catId, expenseList) && catFind.value()->_isShown && j->second->_ownership == -1)
					{
						memberTotal = memberTotal + j->second->_amount;
					}
				}
			}
		}
		membersTotal = membersTotal + memberTotal;
		if (std::abs(memberTotal) > 0)
		{
			QtHouseholdMember* member = new QtHouseholdMember();
			member->_ID = -1;
			member->ui->_memberName->setText("Joint");
			member->ui->_amount->setText("$" + QString::number(memberTotal));
			ui->_householdMembers->layout()->addWidget(member);
			_members.append(member);
		}
		
		
		for (auto u = userMap.begin(); u != userMap.end(); u++)
		{
			memberTotal = 0;
			for (auto i = transDateData.lower_bound(startDate); i->first <= endDate && i != transDateData.end(); i++)
			{
				for (auto j = i->second.begin(); j != i->second.end(); j++)
				{
					auto catFind = catData.find(j->second->_catId);
					if (catFind != catData.end())
					{
						if (StaticController::_accountsController->checkIfExistsInList(j->second->_catId, expenseList) && catFind.value()->_isShown && j->second->_ownership == u.key())
						{
							memberTotal = memberTotal + j->second->_amount;
						}
					}
				}
			}

			membersTotal = membersTotal + memberTotal;
			if (std::abs(memberTotal) > 0)
			{
				QtHouseholdMember* member = new QtHouseholdMember();
				member->_ID = u.key();
				member->ui->_memberName->setText(u.value()->_name);
				member->ui->_amount->setText("$" + QString::number(memberTotal));
				_members.append(member);
				ui->_householdMembers->layout()->addWidget(member);
			}
		}

	}
	ui->debug->setText(isShown);
}

bool QtBudget::ParentCatAlreadyDisplayed(int catId, QList<int> &list)
{
	for (int i = 0; i < list.count(); i++)
	{
		if (catId == list.at(i))
		{
			return true;
		}
	}
	return false;
}

bool QtBudget::CheckIfCatInBudget(int catId)
{
	auto& catMap = StaticController::_accountsController->_categories;
	auto findCat = catMap.find(catId);
	auto& budgetMap = StaticController::_accountsController->_budgets;
	CategoryData* cat = nullptr;
	BudgetData* budget = nullptr;
	CategoryData* budCat = nullptr;
	if (findCat != catMap.end())
	{
		cat = findCat.value();
		for (auto budgetIter = budgetMap.begin(); budgetIter != budgetMap.end(); budgetIter++)
		{
			budget = budgetIter.value();
			auto budgetcatfind = catMap.find(budget->_categoryID);
			if (budgetcatfind != catMap.end())
			{
				budCat = budgetcatfind.value();
				if (cat->_parentID == budCat->_ID || cat->_ID == budCat->_ID) //if the category is a child of the budget cat or the category equal to the budget cat then it is in a budget 
				{
					return true;
				}
			}
		}
	}
	return false;
}

int QtBudget::getCurrentMonthSelection()
{
	int index = ui->_month->currentIndex();
	auto i = _months.find(index + 1);
	if (i != _months.end())
	{
		return i.key();
	}
	else return -1;
}

QString QtBudget::getCurrentMonthSelectionQ()
{
	int index = ui->_month->currentIndex();
	auto i = _months.find(index + 1);
	if (i != _months.end())
	{
		return i.value();
	}
	else return "";
}

int QtBudget::getCurrentYearSelection()
{
	int index = ui->_year->currentIndex();
	auto i = _years.find(index);
	if (i != _years.end())
	{
		return i.value();
	}
	else return -1;
}

bool QtBudget::isTransInBudget(int transCat, QList<int> * catIDs)
{
	for (int i = 0; i < catIDs->size(); i++)
	{
		if (transCat == catIDs->at(i))
		{
			return true;
		}
	}
	return false;
}

QList<int> * QtBudget::includedCatIDs(int budgetCat)
{
	QList<int>* inCatIDs = new QList<int>();
	inCatIDs->append(budgetCat);
	auto& catMap = StaticController::_accountsController->_categories;
	auto findCat = catMap.find(budgetCat);
	if (findCat != catMap.end())
	{
		//StaticController::_mainWindow->_budgets->ui->debug->setText(QString::number(findCat.value()->_parentID));
		int transCatParentID = findCat.value()->_parentID;
		if (transCatParentID <= 0) //no parentID found, this is a parent Category-> to find all children
		{
			for (auto catMapIter = catMap.begin(); catMapIter != catMap.end(); catMapIter++)
			{
				if (catMapIter.value()->_parentID == budgetCat)
				{
					inCatIDs->append(catMapIter.value()->_ID);
				}
			}
		}
	}
	/*
	QString cats;
	for (int i = 0; i < inCatIDs->size(); i++)
	{
		cats = cats + ", " + QString::number(inCatIDs->at(i));
	}
	StaticController::_mainWindow->_budgets->ui->debug->setText(cats);*/
	return inCatIDs;
}

void QtBudget::ClearUI()
{
	for (auto i = _budgetList.begin(); i != _budgetList.end(); i++)
	{
		delete i.value();
	}
	for (int i = 0; i < _incomeLabels.size(); i++)
	{
		delete _incomeLabels.at(i);
	}
	for (int i = 0; i < _otherLabels.size(); i++)
	{
		delete _otherLabels.at(i);
	}
	for (int i = 0; i < _members.size(); i++)
	{
		delete _members.at(i);
	}
	_members.clear();
	_budgetList.clear();
	_incomeLabels.clear();
	_otherLabels.clear();
}
