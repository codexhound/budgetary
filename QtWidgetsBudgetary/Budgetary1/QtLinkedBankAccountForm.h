#pragma once

#include <QWidget>
#include "ui_QtLinkedBankAccountForm.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtLinkedBankAccountFormClass; };
QT_END_NAMESPACE

class QtLinkedBankAccountForm : public QWidget
{
	Q_OBJECT

public:
	QtLinkedBankAccountForm(QWidget *parent = nullptr);
	~QtLinkedBankAccountForm();

private:
	Ui::QtLinkedBankAccountFormClass *ui;
};
