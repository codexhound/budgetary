#include "AccountData.h"

AccountData::AccountData(QObject *parent)
	: QObject(parent)
{
	_ID = 0;
	_name = "";
	_balance = 0.0;
	_linkedBankID = 0;
	_active = true;
}

AccountData::~AccountData()
{}
