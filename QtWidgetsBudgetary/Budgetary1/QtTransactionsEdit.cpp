#include "QtTransactionsEdit.h"
#include "StaticH.h"
#include "qjsonobject.h"

QtTransactionsEdit::QtTransactionsEdit(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtTransactionsEditClass())
{
	ui->setupUi(this);

	_transID = 0;
	_positive = 1;

	ui->_dateEdit->setDate(QDate::currentDate());

	connect(ui->_okButton, &QPushButton::pressed, this, &QtTransactionsEdit::OkPressed);
	connect(ui->_cancelBut, &QPushButton::pressed, this, &QtTransactionsEdit::CancelPressed);
	connect(ui->_deleteTranBut, &QPushButton::pressed, this, &QtTransactionsEdit::DeleteTransactionPressed);
	connect(ui->_splitTransBut, &QPushButton::pressed, this, &QtTransactionsEdit::SplitTransactionPressed);
	
}

void QtTransactionsEdit::ClearUI()
{
	_catComboIndexMap.clear();
	_catComboIndexMapR.clear();
	_accountComboIndexMap.clear();
	_accountComboIndexMapR.clear();
	_ownerComboIndexMap.clear();
	_ownerComboIndexMapR.clear();

	ui->_accountCombo->clear();
	ui->_amountSpin->setValue(0);
	ui->_catCombo->clear();
	ui->_ownerCombo->clear();
}
void QtTransactionsEdit::OnSetupUI()
{
	ClearUI();
	//create the category combo box
	auto& catMap = StaticController::_accountsController->_categories;
	int index = 0;
	for (auto it = catMap.begin(); it != catMap.end(); it++)
	{
		if (it.value()->_parentID <= 0)
		{
			//parent

			ui->_catCombo->addItem(it.value()->_name);
			_catComboIndexMap.insert(index, it.key());
			_catComboIndexMapR.insert(it.key(), index);

			index++;

			for (auto j = catMap.begin(); j != catMap.end(); j++)
			{
				if (j.value()->_parentID == it.key())
				{
					ui->_catCombo->addItem(it.value()->_name + "---->" + j.value()->_name);
					_catComboIndexMap.insert(index, j.key());
					_catComboIndexMapR.insert(j.key(), index);

					index++;
				}
			}
		}
	}
	index = 0;
	auto& accountMap = StaticController::_accountsController->_accounts;
	for (auto it = accountMap.begin(); it != accountMap.end(); it++)
	{
		_accountComboIndexMap.insert(index, it.key());
		_accountComboIndexMapR.insert(it.key(), index);
		ui->_accountCombo->addItem(it.value()->_name);
		index++;
	}

	index = 1;
	_ownerComboIndexMap.insert(0, -1);
	_ownerComboIndexMapR.insert(-1, 0);
	ui->_ownerCombo->addItem("Joint");
	auto& userMap = StaticController::_accountsController->_users;
	for (auto it = userMap.begin(); it != userMap.end(); it++)
	{
		_ownerComboIndexMap.insert(index, it.key());
		_ownerComboIndexMapR.insert(it.key(), index);
		ui->_ownerCombo->addItem(it.value()->_name);
		index++;
	}
	if (_ownerComboIndexMap.size() > 0)
	{
		ui->_ownerCombo->setCurrentIndex(0);
	}
	if (_accountComboIndexMap.size() > 0)
	{
		ui->_accountCombo->setCurrentIndex(0);
	}
	if (_catComboIndexMap.size() > 0)
	{
		ui->_catCombo->setCurrentIndex(0);
	}

	_transID = 0;

	ui->_dateEdit->setDate(QDate::currentDate());
}

void QtTransactionsEdit::CancelPressed()
{
	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}

void QtTransactionsEdit::SplitTransactionPressed()
{
	QtSplitTransaction* splitTran = StaticController::_mainWindow->_splitTransactionForm;
	splitTran->OnSetupUI(_transID);
	StaticController::_mainWindow->showWindow(splitTran);
}
void QtTransactionsEdit::DeleteTransactionPressed()
{
	//delete the transaction
	bool typeError = false;
	bool error = false;
	auto findTrans = StaticController::_accountsController->_transactions.find(_transID);
	if (findTrans != StaticController::_accountsController->_transactions.end())
	{
		auto findAccount = StaticController::_accountsController->_accounts.find(findTrans.value()->_accountId);
		if (findAccount != StaticController::_accountsController->_accounts.end())
		{
			if (findAccount.value()->_linkedBankID > 0) //not allowed to delete linked transactions
			{
				typeError = true;
			}
		}
	}
	error = typeError;

	if (!error) //if there are no errors
	{
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = _transID;
		data["isActive"] = 0;
		data["accountId"] = 0;
		data["catID"] = 0;
		data["owner"] = 0;
		data["amount"] = 0;
		data["date"] = "";
		data["name"] = "";
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateTransaction";
		StaticController::Post(url, obj);
	}

	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}
void QtTransactionsEdit::OkPressed()
{
	//must process this
	QDate date = ui->_dateEdit->date();
	
	QString name = ui->_name->text();
	int catId = _catComboIndexMap[ui->_catCombo->currentIndex()];
	int accountId = _accountComboIndexMap[ui->_accountCombo->currentIndex()];
	int ownerId = _ownerComboIndexMap[ui->_ownerCombo->currentIndex()];
	int type = ui->_typeCombo->currentIndex();
	int ID = _transID;
	double positive = 1;

	if (type == 1) //credit, make amount negative
	{
		positive = -1;
	}
	double amount = ui->_amountSpin->value() * positive;

	auto& tranMap = StaticController::_accountsController->_transactions;
	auto findTrans = tranMap.find(ID);
	double dataAmount = 0;
	QDate dataDate;
	int dataAccountId = 0;
	if (findTrans != tranMap.end())
	{
		dataAmount = findTrans.value()->_amount;
		dataDate = findTrans.value()->_date;
		dataAccountId = findTrans.value()->_accountId;

	}

	auto& accountMap = StaticController::_accountsController->_accounts;
	auto findAccount = accountMap.find(accountId);
	bool accountTypeError = false;
	ui->debug->setText("Data Amount: " + QString::number(dataAmount) + ", " + "UI Amount: " + QString::number(amount) + ", Data Account ID: " + QString::number(dataAccountId) + ", UI Account ID: " + QString::number(accountId));
	if (findAccount != accountMap.end())
	{
		//need to round the data amount to nearest ui amount
		if (ID <= 0 || (ID > 0 && (dataAmount != amount || date != dataDate || accountId != dataAccountId))) //check if this is a valid account, can only create transactions or edit transaction amounts on manual accounts
		{
			if (findAccount.value()->_linkedBankID > 0) //this is a linked(non manual account)
			{
				//so reset the invalid changes back to whats in the data
				accountId = dataAccountId;
				date = dataDate;
				amount = dataAmount;

				if (ID <= 0) //creating transaction on linked account, this is disallowed
				{
					accountTypeError = true;
				}
			}
		}
	}

	if (catId > 0 && std::abs(amount) > 0 && name != "" && accountId > 0 && ownerId >= -1 && !accountTypeError)
	{
		//update the server
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = _transID;
		data["isActive"] = 1;
		data["accountId"] = accountId;
		data["catID"] = catId;
		data["owner"] = ownerId;
		data["amount"] = amount;
		data["date"] = date.toString(Qt::ISODate);
		data["name"] = name;
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateTransaction";
		StaticController::Post(url, obj);
	}
	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}

QtTransactionsEdit::~QtTransactionsEdit()
{
	delete ui;
}
