#pragma once

#include <QWidget>
#include "ui_QtHouseholdMember.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtHouseholdMemberClass; };
QT_END_NAMESPACE

class QtHouseholdMember : public QWidget
{
	Q_OBJECT

public:
	QtHouseholdMember(QWidget *parent = nullptr);
	~QtHouseholdMember();

	Ui::QtHouseholdMemberClass* ui;

	int _ID;

public slots:
	void MemberButtonPressed();

private:
	
};
