#pragma once

#include <QWidget>
#include "ui_QtAccountForm.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtAccountFormClass; };
QT_END_NAMESPACE

class QtAccountForm : public QWidget
{
	Q_OBJECT

public:
	QtAccountForm(QWidget *parent = nullptr);
	~QtAccountForm();

	int _ID;

	Ui::QtAccountFormClass* ui;

	void UpdateUI();
	void ClearUI();

public slots:
	void removeAccount();
	void viewTransactions();
	void OkPressed();
	void CancelPressed();

private:
	
	
};
