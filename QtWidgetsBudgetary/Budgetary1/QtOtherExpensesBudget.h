#pragma once

#include <QWidget>
#include "ui_QtOtherExpensesBudget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtOtherExpensesBudgetClass; };
QT_END_NAMESPACE

class QtOtherExpensesBudget : public QWidget
{
	Q_OBJECT

public:
	QtOtherExpensesBudget(QWidget *parent = nullptr);
	~QtOtherExpensesBudget();

	Ui::QtOtherExpensesBudgetClass* ui;

	int _catId;

public slots:
	void ViewTransactionsPressed();
	void CreateBudgetPressed();

private:
	
};
