#pragma once

#include <QWidget>
#include "ui_QtEditCategory.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtEditCategoryClass; };
QT_END_NAMESPACE

class QtEditCategory : public QWidget
{
	Q_OBJECT

		

public:
	QtEditCategory(QWidget *parent = nullptr);
	~QtEditCategory();

	Ui::QtEditCategoryClass* ui;

	int _ID;

	QMap<int, int> _catComboMap;
	QMap<int, int> _catComboMapR;

	void UpdateUI();
	void ClearUI();

public slots:
	void OkPressed();
	void CancelPressed();
	void DeletePressed();

private:
	
};
