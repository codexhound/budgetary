#pragma once

#include <QWidget>
#include "ui_QtBudgetList.h"
#include <QDate>

QT_BEGIN_NAMESPACE
namespace Ui { class QtBudgetListClass; };
QT_END_NAMESPACE

class QtBudgetList : public QWidget
{
	Q_OBJECT

public:
	QtBudgetList(QWidget *parent = nullptr);
	~QtBudgetList();

	Ui::QtBudgetListClass* ui;

	int _ID; //budget id
	QDate _startDate;
	QDate _endDate;
	QList<int> _catList; //included category ids


public slots:
	void OnViewEditPressed();
	void OnViewTransactionsPressed();

private:
	
};
