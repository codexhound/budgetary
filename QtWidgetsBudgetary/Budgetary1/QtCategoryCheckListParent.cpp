#include "QtCategoryCheckListParent.h"
#include "StaticH.h"

QtCategoryCheckListParent::QtCategoryCheckListParent(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtCategoryCheckListParentClass())
{
	ui->setupUi(this);

	connect(ui->_parentCheckBox, &QCheckBox::stateChanged, this, &QtCategoryCheckListParent::OnCheckBoxChanged);

	_ID = 0;
}

void QtCategoryCheckListParent::OnCheckBoxChanged()
{
	bool isChecked = ui->_parentCheckBox->isChecked();
	if (!isChecked)
	{
		StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(false);
		StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setChecked(false);
		StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(true);
	}
	else
	{
		if (StaticController::_mainWindow->_filterTransactionCategories->isAllChecked())
		{
			StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(false);
			StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setChecked(true);
			StaticController::_mainWindow->_filterTransactionCategories->ui->_checkAll->setEnabled(true);
		}
	}

	for (auto it = _childMap.begin(); it != _childMap.end(); it++)
	{
		it.value()->ui->_checkBox->setChecked(isChecked);
	}
}

QtCategoryCheckListParent::~QtCategoryCheckListParent()
{
	delete ui;
}
