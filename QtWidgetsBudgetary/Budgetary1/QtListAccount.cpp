#include "QtListAccount.h"
#include "StaticH.h"

QtListAccount::QtListAccount(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtListAccountClass())
{
	ui->setupUi(this);

	_ID = 0;
	connect(ui->_View_Edit, &QPushButton::pressed, this, &QtListAccount::ViewEditPressed);
	connect(ui->_Delete, &QPushButton::pressed, this, &QtListAccount::DeletePressed);
}

QtListAccount::~QtListAccount()
{
	delete ui;
}

void QtListAccount::ViewEditPressed()
{
	QtAccountForm* accountEdit = StaticController::_mainWindow->_accountForm;
	accountEdit->_ID = _ID;
	accountEdit->UpdateUI();
	StaticController::_mainWindow->showWindow(accountEdit);
}

void QtListAccount::DeletePressed()
{
	QtAccountForm* accountEdit = StaticController::_mainWindow->_accountForm;
	accountEdit->_ID = _ID;
	accountEdit->removeAccount();
	accountEdit->ClearUI();
}
