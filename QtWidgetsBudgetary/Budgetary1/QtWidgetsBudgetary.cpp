#include "QtWidgetsBudgetary.h"
#include <QtWebEngineWidgets/qwebengineview.h>
#include <QtNetwork>
#include "StaticH.h"
#include "Enums.h"


QtWidgetsBudgetary::QtWidgetsBudgetary(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::QtWidgetsBudgetaryClass())
{
    ui->setupUi(this);

    _updateCounter = 0;

    _networkManager = new QNetworkAccessManager(this);
    _webView = new QWebEngineView(this);
    _webViewWindow = new QWebEngineView(this);

    _accountController = new AccountController(this);

    _accounts = new QtAccounts(this);
    _budgets = new QtBudget(this);
    _linkedBanks = new QtLinkedBanks(this);
    _transactions = new QtTransactions(this);
    _categories = new QtListCategories(this);
    _pickLinkName = new QtPickLinkedAccountName(this);
    _editCategory = new QtEditCategory(this);
    _editTransactions = new QtTransactionsEdit(this);
    _budgetEdit = new QtBudgetEdit(this);
    _accountForm = new QtAccountForm(this);
    _linkedBankAccountForm = new QtLinkedBankAccountForm(this);
    _linkedBankForm = new QtLinkedBankForm(this);
    _splitTransactionForm = new QtSplitTransaction(this);
    _filterTransactionCategories = new QtFilterCategory(this);
    _createTransRule = new QtCreateTransactionRule(this);
    _chooseAccountType = new QtChooseAccountTypeCreate(this);

    ui->_mainWidgets->layout()->addWidget(_accounts);
    ui->_mainWidgets->layout()->addWidget(_budgets);
    ui->_mainWidgets->layout()->addWidget(_linkedBanks);
    ui->_mainWidgets->layout()->addWidget(_transactions);
    ui->_mainWidgets->layout()->addWidget(_categories);
    ui->_mainWidgets->layout()->addWidget(_webView);
    ui->_mainWidgets->layout()->addWidget(_webViewWindow);
    ui->_mainWidgets->layout()->addWidget(_pickLinkName);
    ui->_mainWidgets->layout()->addWidget(_editCategory);
    ui->_mainWidgets->layout()->addWidget(_editTransactions);
    ui->_mainWidgets->layout()->addWidget(_budgetEdit);
    ui->_mainWidgets->layout()->addWidget(_accountForm);
    ui->_mainWidgets->layout()->addWidget(_linkedBankAccountForm);
    ui->_mainWidgets->layout()->addWidget(_linkedBankForm);
    ui->_mainWidgets->layout()->addWidget(_splitTransactionForm);
    ui->_mainWidgets->layout()->addWidget(_filterTransactionCategories);
    ui->_mainWidgets->layout()->addWidget(_createTransRule);
    ui->_mainWidgets->layout()->addWidget(_chooseAccountType);

    _widgets.append(_accounts);
    _widgets.append(_budgets);
    _widgets.append(_linkedBanks);
    _widgets.append(_transactions);
    _widgets.append(_categories);
    _widgets.append(_webView);
    _widgets.append(_webViewWindow);
    _widgets.append(_pickLinkName);
    _widgets.append(_editCategory);
    _widgets.append(_editTransactions);
    _widgets.append(_budgetEdit);
    _widgets.append(_accountForm);
    _widgets.append(_linkedBankForm);
    _widgets.append(_linkedBankAccountForm);
    _widgets.append(_splitTransactionForm);
    _widgets.append(_filterTransactionCategories);
    _widgets.append(_createTransRule);
    _widgets.append(_chooseAccountType);

    _currentlyShowingWindow = nullptr;
    _prevShowingWindow = nullptr;
    showWindow(_accounts);

    connect(ui->actionAccounts, &QAction::triggered, this, &QtWidgetsBudgetary::actionAccountsClicked);
    connect(ui->actionBudget, &QAction::triggered, this, &QtWidgetsBudgetary::actionBudgetsClicked);
    connect(ui->actionCategories, &QAction::triggered, this, &QtWidgetsBudgetary::actionCategoriesClicked);
    connect(ui->actionLinked_Banks, &QAction::triggered, this, &QtWidgetsBudgetary::actionLinkedBanksClicked);
    connect(ui->actionTransactions, &QAction::triggered, this, &QtWidgetsBudgetary::actionTransactionsClicked);

    connect(_networkManager, &QNetworkAccessManager::finished, this, &QtWidgetsBudgetary::replyFinished);

    connect(this, &QtWidgetsBudgetary::DBFinishedUpdating, this, &QtWidgetsBudgetary::OnDBFinishedUpdating);

    StaticController::_accountsController = _accountController;
    StaticController::_networkManager = _networkManager;
    _getDataCounter = 0;

    //Update Accounts, Transactions, and master API categories
    QString url = "http://WIN-Q75VOTO0D0V:80/api/accounts";
    StaticController::Get(url);

    url = "http://WIN-Q75VOTO0D0V:80/api/transactions";
    StaticController::Get(url);

    url = "http://WIN-Q75VOTO0D0V:80/api/updateCategories";
    StaticController::Get(url);
}

void QtWidgetsBudgetary::OnDBAPIUpdated()
{
    QString url = "http://WIN-Q75VOTO0D0V:80/api/removeold";
    StaticController::Get(url);
}

void QtWidgetsBudgetary::OnDBFinishedUpdating()
{
    ClearUI();
    StaticController::_accountsController->ClearData();
    ui->lineEdit->setText("DB Updates Done");
    //request initial account info
    QJsonObject obj;
    obj["username"] = "mjbourquin";
    QString url = "http://WIN-Q75VOTO0D0V:80/getAccountsDB";
    StaticController::Get(url);

    //request initial linked account info
    obj["username"] = "mjbourquin";
    url = "http://WIN-Q75VOTO0D0V:80/getLinkedBanksDB";
    StaticController::Get(url);

    obj["username"] = "mjbourquin";
    url = "http://WIN-Q75VOTO0D0V:80/getCategoriesDB";
    StaticController::Get(url);

    obj["username"] = "mjbourquin";
    url = "http://WIN-Q75VOTO0D0V:80/getBudgetsDB";
    StaticController::Get(url);

    obj["username"] = "mjbourquin";
    url = "http://WIN-Q75VOTO0D0V:80/getTransactionsDB";
    StaticController::Get(url);
}

void QtWidgetsBudgetary::ClearUI()
{
    _accounts->ClearAccountsList();
    _transactions->ClearUI();
    _linkedBanks->ClearUI();
    _budgets->ClearUI();
    _categories->ClearUI();
}

void QtWidgetsBudgetary::ReplyErrorOccured(QNetworkReply::NetworkError error)
{
    ui->lineEdit->setText("Error Occured");
    ui->lineEdit->setText(QString::number(error));
}

void QtWidgetsBudgetary::ReplyCheck()
{
    ui->lineEdit->setText("Reply Sending");
}

void QtWidgetsBudgetary::SendRequest()
{
    ui->lineEdit->setText("Sending Request");
    //QNetworkRequest* request = new QNetworkRequest(QUrl("http://WIN-Q75VOTO0D0V:80/api/getLatestLinkedAccount"));
    QNetworkRequest* request = new QNetworkRequest(QUrl("http://google.com"));
    QNetworkReply* reply = _networkManager->get(*request);
        ui->lineEdit->setText(reply->readAll());
}

void QtWidgetsBudgetary::UpdateUIData(bool fullRefresh)
{
    emit _accounts->UpdateAccountsUI();

    //check all the categories in the transactions filter
    if (fullRefresh)
    {
        _filterTransactionCategories->CheckAll();
        _transactions->SetAllOwners();
    }
    emit _transactions->UpdateTransactionsUI();

    emit _budgets->UpdateBudgetUI();
    emit _linkedBanks->UpdateLinkedBankUI();
    emit _categories->UpdateCategoriesUI();
}

void QtWidgetsBudgetary::replyFinished(QNetworkReply* reply){
    //ui->lineEdit->setText("Manager Finished");
    if (reply != nullptr) {
        if (reply->error()) {
            ui->lineEdit->setText(QString::number(reply->error()));
        }
        else
        {
            QByteArray temp = reply->readAll();
            QString answer = QString(temp);

            //ui->lineEdit->setText(answer);

            QJsonDocument doc = QJsonDocument::fromJson(temp);
            QJsonObject json = doc.object();

            QJsonArray data;
            QJsonObject datarow;

            AccountController* controller = StaticController::_accountsController;

           
            QString RequestType = json["RequestType"].toString();
            if (RequestType == "LatestLinkedAccount")
            {
                int LinkEventValue = json["LinkEvent"].toInt();
                if (LinkEventValue)
                {
                    ui->lineEdit->setText("Got a Link Event");
                    int AccountId = json["AccountId"].toInt();
                    int UserId = json["UserId"].toInt();
                    int ValidAdd = json["ValidAdd"].toInt();
                    if (ValidAdd)
                    {
                        //need to set the name
                        _pickLinkName->_ID = AccountId;
                        this->showWindow(_pickLinkName);
                    }
                    else
                    {
                        //not a valid add but done, show accounts again
                        this->showWindow(_accounts);
                    }
                    emit CheckForLinksFinished(); //stops the checks for if the link is finished
                }
                else
                {
                    ui->lineEdit->setText("No Link Events");
                }
            }
            else if (RequestType == "RemoveOld")
            {
                OnDBFinishedUpdating();
            }
            //emit signal when both of these are done. Can update retrieve accounts and transaction data again
            else if (RequestType == "UpdateTransactions")
            {
                _updateCounter++;
                if (_updateCounter > 2)
                {
                    OnDBAPIUpdated();
                    _updateCounter = 0;
                }
            }
            else if (RequestType == "UpdateAccounts")
            {
                _updateCounter++;
                if (_updateCounter > 2)
                {
                    OnDBAPIUpdated();
                    _updateCounter = 0;
                }
            }
            else if (RequestType == "UpdateMasterCategories")
            {
                _updateCounter++;
                if (_updateCounter > 2)
                {
                    OnDBAPIUpdated();
                    _updateCounter = 0;
                }
            }
            else if (RequestType == "UpdateMultiTransaction")
            {
                ClearUI();

                int ID;
                double amount;
                QString name;
                QDate date;
                bool isActive;
                Enums::Ownership ownership1;
                int userId;
                int catId;
                int accountId;

                auto& tranDateMap = controller->_transByDate;
                auto& tranMap = controller->_transactions;
                auto findTran = tranMap.find(ID);

                data = json["Data"].toArray();
                TransactionData* transaction = nullptr;

                

                for (int i = 0; i < data.size(); i++)
                {
                    datarow = data.at(i).toObject();

                    ID = datarow["ID"].toInt();
                    amount = datarow["amount"].toDouble();
                    name = datarow["name"].toString();
                    date = QDate::fromString(datarow["date"].toString(), Qt::ISODate);
                    userId = datarow["userId"].toInt();
                    ownership1 = (Enums::Ownership)datarow["ownership"].toInt();
                    catId = datarow["catId"].toInt();
                    isActive = datarow["isActive"].toInt();
                    accountId = datarow["accountId"].toInt();

                    

                    findTran = tranMap.find(ID);

                    if (findTran != tranMap.end()) //update
                    {
                        transaction = findTran.value();
                        if (isActive)
                        {
                            transaction->_amount = amount;
                            transaction->_name = name;
                            transaction->_date = date;
                            transaction->_userId = userId;
                            transaction->_catId = catId;
                            transaction->_ownership = ownership1;
                            transaction->_isActive = true;
                            transaction->_accountId = accountId;
                        }
                        else
                        {
                            tranMap.remove(ID);

                            //remove from the date map too
                            auto findDate = tranDateMap.find(transaction->_date);
                            if (findDate != tranDateMap.end())
                            {
                                auto findTran1 = findDate->second.find(ID);
                                if (findTran1 != findDate->second.end())
                                {
                                    findDate->second.erase(ID);
                                    if (findDate->second.size() == 0) //if the internal map is now size 0 remove the date from the map also
                                    {
                                        tranDateMap.erase(transaction->_date);
                                    }
                                }
                            }
                            delete transaction;
                        }
                    }
                    else //add new
                    {
                        transaction = new TransactionData(controller);
                        transaction->_ID = ID;
                        transaction->_amount = amount;
                        transaction->_name = name;
                        transaction->_catId = catId;
                        transaction->_date = date;
                        transaction->_userId = userId;
                        transaction->_ownership = ownership1;
                        transaction->_isActive = true;
                        transaction->_accountId = accountId;

                        controller->_transactions.insert(ID, transaction);
                        auto dateFind = controller->_transByDate.find(date);
                        if (dateFind == controller->_transByDate.end()) //doesn't exist, create
                        {
                            controller->_transByDate[date] = std::map<int, TransactionData*>();
                        }
                        controller->_transByDate[date][ID] = transaction;
                    }
                }
                //now clear and update the UI
                UpdateUIData();
            }
            else if (RequestType == "UpdateAccount")
            {
            ClearUI();

            int ID;
            QString name;
            double balance;
            int isActive;

            ID = json["ID"].toInt();
            name = json["name"].toString();
            isActive = json["isActive"].toInt();
            balance = json["balance"].toDouble();

            AccountData* account = nullptr;

            auto& accountMap = controller->_accounts;
            auto findAccount = accountMap.find(ID);

            if (findAccount != accountMap.end()) //update
            {
                account = findAccount.value();
                if (isActive)
                {
                    account->_balance = balance;
                    account->_name = name;
                }
                else
                {
                    accountMap.remove(ID);
                    delete account;
                }
            }
            else //add new
            {
                account = new AccountData(this);
                account->_ID = ID;
                account->_name = name;
                account->_balance = balance;
                account->_linkedBankID = 0;
                account->_active = 1;
                controller->_accounts.insert(ID, account);
            }
            //now clear and update the UI
            UpdateUIData();
            }
            else if (RequestType == "UpdateBudget")
            {
            QString dataelements = "";
            ClearUI();

            int ID;
            double amount;
            int catId;
            QString name;
            int isActive;

            ID = json["ID"].toInt();
            amount = json["amount"].toDouble();
            name = json["name"].toString();
            catId = json["catId"].toInt();
            isActive = json["isActive"].toInt();

            BudgetData * budget = nullptr;

            auto& budgetMap = controller->_budgets;
            auto findBudget = budgetMap.find(ID);

            if (findBudget != budgetMap.end()) //update
            {
                budget = findBudget.value();
                if (isActive)
                {
                    budget->_amount = amount;
                    budget->_name = name;
                    budget->_categoryID = catId;
                    budget->_isActive = true;
                }
                else
                {
                    budgetMap.remove(ID);
                    delete budget;
                }
            }
            else //add new
            {
                budget = new BudgetData(controller);
                budget->_ID = ID;
                budget->_categoryID = catId;
                budget->_amount = amount;
                budget->_name = name;
                budget->_isActive = true;

                controller->_budgets.insert(ID, budget);
            }
            //now clear and update the UI
            UpdateUIData();
            }
            else if (RequestType == "UpdateCategory")
            {
                ClearUI();

                int ID;
                int parentID;
                QString name;
                bool isActive;
                bool isShown;
                int masterId;

                ID = json["ID"].toInt();
                parentID = json["parentId"].toDouble();
                name = json["name"].toString();
                isActive = json["isActive"].toInt();
                isShown = json["Show"].toInt();
                masterId = json["MasterId"].toInt();

                CategoryData* category = nullptr;

                auto& catMap = controller->_categories;
                auto findTran = catMap.find(ID);

                if (findTran != catMap.end()) //update
                {
                    category = findTran.value();
                    if (isActive)
                    {
                        category->_isActive = isActive;
                        category->_parentID = parentID;
                        category->_isShown = isShown;
                        category->_masterId = masterId;
                        category->_name = name;
                    }
                    else
                    {
                        catMap.remove(ID);
                        delete category;
                    }
                }
                else //add new
                {
                    category = new CategoryData(controller);
                    category->_ID = ID;
                    category->_isActive = isActive;
                    category->_parentID = parentID;
                    category->_isShown = isShown;
                    category->_masterId = masterId;
                    category->_name = name;

                    controller->_categories.insert(ID, category);
                }

                UpdateUIData();
            }
            else if (RequestType == "UpdateTransaction")
            {
                QString dataelements = "";
                ClearUI();

                int ID;
                double amount;
                QString name;
                QDate date;
                bool isActive;
                Enums::Ownership ownership1;
                int userId;
                int catId;
                int accountId;

                ID = json["ID"].toInt();
                amount = json["amount"].toDouble();
                name = json["name"].toString();
                date = QDate::fromString(json["date"].toString(), Qt::ISODate);
                userId = json["userId"].toInt();
                ownership1 = (Enums::Ownership)json["ownership"].toInt();
                catId = json["catId"].toInt();
                isActive = json["isActive"].toInt();
                accountId = json["accountId"].toInt();

                TransactionData* transaction = nullptr;

                auto& tranDateMap = controller->_transByDate;
                auto& tranMap = controller->_transactions;
                auto findTran = tranMap.find(ID);
                
                if (findTran != tranMap.end()) //update
                {
                    transaction = findTran.value();
                    if (isActive)
                    {
                        transaction->_amount = amount;
                        transaction->_name = name;
                        transaction->_date = date;
                        transaction->_catId = catId;
                        transaction->_userId = userId;
                        transaction->_ownership = ownership1;
                        transaction->_isActive = true;
                        transaction->_accountId = accountId;
                    }
                    else
                    {
                        tranMap.remove(ID);
                       
                        //remove from the date map too
                        auto findDate = tranDateMap.find(transaction->_date);
                        if (findDate != tranDateMap.end())
                        {
                            auto findTran1 = findDate->second.find(ID);
                            if (findTran1 != findDate->second.end())
                            {
                                findDate->second.erase(ID);
                                if (findDate->second.size() == 0) //if the internal map is now size 0 remove the date from the map also
                                {
                                    tranDateMap.erase(transaction->_date);
                                }
                            }
                        }
                        delete transaction;
                    }
                }
                else //add new
                {
                    transaction = new TransactionData(controller);
                    transaction->_ID = ID;
                    transaction->_catId = catId;
                    transaction->_amount = amount;
                    transaction->_name = name;
                    transaction->_date = date;
                    transaction->_userId = userId;
                    transaction->_ownership = ownership1;
                    transaction->_isActive = true;
                    transaction->_accountId = accountId;

                    controller->_transactions.insert(ID, transaction);
                    auto dateFind = controller->_transByDate.find(date);
                    if (dateFind == controller->_transByDate.end()) //doesn't exist, create
                    {
                        controller->_transByDate[date] = std::map<int, TransactionData*>();
                    }
                    controller->_transByDate[date][ID] = transaction;
                    for (auto it = controller->_transByDate[date].begin(); it != controller->_transByDate[date].end(); it++)
                    {
                        dataelements = dataelements + QString::number(it->first) + ", ";
                    }
                }
                ui->lineEdit->setText(dataelements);
                //now clear and update the UI
                UpdateUIData();
            }
            else if (RequestType == "GetAccountInfo")
            {
                //load the account info into the data controller
                data = json["Data"].toArray();

                AccountData* account = nullptr;

                int accountId;
                QString uniqueName;
                QString accountType;
                double accountBalance;
                int houseId;
                int userOwnerId;
                QString userOwnerName;
                int privateUserId;
                QString privateUserName;
                int accountViewable;
                QString externalName;
                QString fullExternalName;
                int parentAccountId;
                int isActive;

                for (int i = 0; i < data.size(); i++)
                {
                    datarow = data.at(i).toObject();

                    accountId = datarow["AccountId"].toInt();
                    uniqueName = datarow["UniqueName"].toString();
                    accountType = datarow["AccountType"].toString();
                    accountBalance = datarow["Amount"].toDouble();
                    houseId = datarow["HouseholdId"].toInt();
                    userOwnerId = datarow["UserOwnerId"].toInt();
                    userOwnerName = datarow["UserOwnerName"].toString();
                    privateUserId = datarow["PrivateUserId"].toInt();
                    privateUserName = datarow["PrivateName"].toString();
                    accountViewable = datarow["ActViewable"].toInt();
                    externalName = datarow["ExternalName"].toString();
                    fullExternalName = datarow["FullExternalName"].toString();
                    parentAccountId = datarow["ParentAccountID"].toInt();
                    isActive = datarow["ParentAccountID"].toInt();

                    
                    if (controller != nullptr)
                    {
                        account = new AccountData(controller);
                        account->_active = isActive;
                        account->_balance = accountBalance;
                        account->_ID = accountId;
                        account->_linkedBankID = parentAccountId;
                        account->_name = uniqueName;
                        controller->_accounts.insert(accountId, account);
                    }
                }

                //update accounts UI
                ui->lineEdit->setText(RequestType);

                _getDataCounter++;
                if (_getDataCounter > 4)
                {
                    UpdateUIData(true);
                    _getDataCounter = 0;
                }
            }
            else if (RequestType == "GetLinkedBankInfo")
            {
                //load the linked info into the data controller
                data = json["Data"].toArray();
                LinkedBankData* linkedBank = nullptr;

                int Id;
                QString name;
                int isActive;


                for (int i = 0; i < data.size(); i++)
                {
                    datarow = data.at(i).toObject();

                    Id = datarow["AccountId"].toInt();
                    name = datarow["LinkedName"].toString();
                    isActive = datarow["isActive"].toInt();


                    if (controller != nullptr)
                    {
                        linkedBank = new LinkedBankData(controller);
                        linkedBank->_active = isActive;
                        linkedBank->_ID = Id;
                        linkedBank->_name = name;
                        linkedBank->_linked = true; //default for now

                        controller->_linkedBanks.insert(Id, linkedBank);
                    }
                }
                _getDataCounter++;
                if (_getDataCounter > 4)
                {
                    UpdateUIData(true);
                    _getDataCounter = 0;
                }
            }
            else if (RequestType == "GetTransactionInfo")
            {
                //load the linked info into the data controller
                data = json["Data"].toArray();
                TransactionData* transaction = nullptr;

                int ID;
                double amount;
                QString name;
                QString externalName;
                QDate date;
                QDate authdate;
                bool isActive;
                bool isPending;
                Enums::Ownership ownership1;
                int userId;
                int catId;
                int accountId;
    
    
                for (int i = 0; i < data.size(); i++)
                {
                    datarow = data.at(i).toObject();
    
                    ID = datarow["ID"].toInt();
                    amount = datarow["amount"].toDouble();
                    name = datarow["name"].toString();
                    externalName = datarow["externalName"].toString();
                    date = QDate::fromString(datarow["date"].toString(), Qt::ISODate);
                    authdate = QDate::fromString(datarow["date"].toString(), Qt::ISODate);
                    isActive = datarow["isActive"].toInt();
                    isPending = datarow["isPending"].toInt();
                    ownership1 = (Enums::Ownership)datarow["ownership"].toInt();
                    userId = datarow["userId"].toInt();
                    catId = datarow["catId"].toInt();
                    accountId = datarow["accountId"].toInt();
                
    
                    if (controller != nullptr)
                    {
                        transaction = new TransactionData(controller);
                        transaction->_amount = amount;
                        transaction->_ID = ID;
                        transaction->_name = name;
                        transaction->_externalName = externalName;
                        transaction->_date = date;
                        transaction->_authdate = authdate;
                        transaction->_isActive = isActive;
                        transaction->_isPending = isPending;
                        transaction->_ownership = ownership1;
                        transaction->_userId = userId;
                        transaction->_catId = catId;
                        transaction->_accountId = accountId;
    
                        controller->_transactions.insert(ID, transaction);
                        auto dateFind = controller->_transByDate.find(date);
                        if (dateFind == controller->_transByDate.end()) //doesn't exist, create
                        {
                            controller->_transByDate[date] = std::map<int, TransactionData*>();
                        }
                        controller->_transByDate[date][ID] = transaction;
                    }
                }
                _getDataCounter++;
                if (_getDataCounter > 4)
                {
                    UpdateUIData(true);
                    _getDataCounter = 0;
                }
            }
            else if (RequestType == "GetCategoryInfo")
            {
            QString categories;
                //load the linked info into the data controller
                data = json["Data"].toArray();
                CategoryData* category = nullptr;

                int ID;
                int parentID;
                QString name;
                bool isActive;
                bool isShown;
                int masterId;

                QString isShownQ;
    
                for (int i = 0; i < data.size(); i++)
                {
                    datarow = data.at(i).toObject();
    
                    ID = datarow["ID"].toInt();
                    parentID = datarow["parentId"].toInt();
                    name = datarow["name"].toString();
                    isActive = datarow["isActive"].toInt();
                    isShown = datarow["isShown"].toInt();
                    masterId = datarow["MasterId"].toInt();

                    categories = categories + QString::number(ID) + ",";

                    isShownQ = isShownQ + ", " + QString::number(isShown);
    
                    if (controller != nullptr)
                    {
                        category = new CategoryData(controller);
                        category->_ID = ID;
                        category->_parentID = parentID;
                        category->_name = name;
                        category->_isActive = isActive;
                        category->_isShown = isShown;
                        category->_masterId = masterId;
    
                        controller->_categories.insert(ID, category);
                    }
                }
                ui->lineEdit->setText(isShownQ);
                _getDataCounter++;
                if (_getDataCounter > 4)
                {
                    UpdateUIData(true);
                    _getDataCounter = 0;
                }

                ui->lineEdit_2->setText(categories);
            }
            else if (RequestType == "GetBudgetInfo")
            {
                //load the linked info into the data controller
                data = json["Data"].toArray();
                BudgetData* budget = nullptr;

                int ID;
                int categoryID;
                QString name;
                Enums::BudgetType type;
                double amount;
                int startingDay;
                bool isActive;
    
                for (int i = 0; i < data.size(); i++)
                {
                    datarow = data.at(i).toObject();
    
                    ID = datarow["ID"].toInt();
                    categoryID = datarow["categoryId"].toInt();
                    qDebug() << categoryID;
                    name = datarow["name"].toString();
                    type = (Enums::BudgetType)datarow["type"].toInt();
                    amount = datarow["amount"].toDouble();
                    startingDay = datarow["startingDay"].toInt();
                    isActive = datarow["isActive"].toInt();
    
                    if (controller != nullptr)
                    {
                        budget = new BudgetData(controller);
                        budget->_ID = ID;
                        budget->_categoryID = categoryID;
                        budget->_name = name;
                        budget->_amount = amount;
                        budget->_startingDay = startingDay;
                        budget->_isActive = isActive;
    
                        controller->_budgets.insert(ID, budget);
                    }
                }
                _getDataCounter++;
                if (_getDataCounter > 4)
                {
                    UpdateUIData(true);
                    _getDataCounter = 0;
                }
            }
        }
        
    }
}


void QtWidgetsBudgetary::SetDebugText(QString data)
{
    ui->lineEdit->setText(data);
}

void QtWidgetsBudgetary::ShowPrevWindow()
{
    if (_prevShowingWindow != nullptr)
    {
        showWindow(_prevShowingWindow);
    }
}

void QtWidgetsBudgetary::showWindow(QWidget* pointer)
{
    _prevShowingWindow = _currentlyShowingWindow;
    if (pointer != nullptr)
    {
        for (int i = 0; i < _widgets.length(); i++)
        {
            if (pointer != _widgets[i] && _widgets[i] != nullptr)
            {
                _widgets[i]->hide();
            }
        }
        pointer->show();
        _currentlyShowingWindow = pointer;
    }
}

QtWidgetsBudgetary::~QtWidgetsBudgetary()
{
    delete ui;
}

void QtWidgetsBudgetary::actionAccountsClicked()
{
    showWindow(_accounts);
}
void QtWidgetsBudgetary::actionTransactionsClicked()
{
    showWindow(_transactions);
}
void QtWidgetsBudgetary::actionBudgetsClicked()
{
    showWindow(_budgets);
}
void QtWidgetsBudgetary::actionLinkedBanksClicked()
{
    showWindow(_linkedBanks);
}
void QtWidgetsBudgetary::actionCategoriesClicked()
{
    showWindow(_categories);
}
