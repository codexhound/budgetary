#pragma once

#include <QWidget>
#include "ui_QtPickLinkedAccountName.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtPickLinkedAccountNameClass; };
QT_END_NAMESPACE

class QtPickLinkedAccountName : public QWidget
{
	Q_OBJECT

public:
	QtPickLinkedAccountName(QWidget *parent = nullptr);
	~QtPickLinkedAccountName();

	int _ID;

public slots:
	void choseName();

private:
	Ui::QtPickLinkedAccountNameClass *ui;
};
