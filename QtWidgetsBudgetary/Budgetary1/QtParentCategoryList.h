#pragma once

#include <QWidget>
#include "QtChildCatList.h"
#include "ui_QtParentCategoryList.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtParentCategoryListClass; };
QT_END_NAMESPACE

class QtParentCategoryList : public QWidget
{
	Q_OBJECT

public:
	QtParentCategoryList(QWidget *parent = nullptr);
	~QtParentCategoryList();

	QMap<int, QtChildCatList*> _childCatWidgets;

	int _ID;

	void UpdateUI();

	Ui::QtParentCategoryListClass* ui;

	void EditPressed();

private:
	
};
