#pragma once

#include <QWidget>
#include "ui_QtTransactions.h"
#include "QtTransaction.h"
#include <qmap.h>
#include "QtTransactionPage.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtTransactionsClass; };
QT_END_NAMESPACE

class QtTransactions : public QWidget
{
	Q_OBJECT

public:
	QtTransactions(QWidget *parent = nullptr);
	~QtTransactions();

	QMap<int, QtTransaction*> _transactions;
	QList<QtTransactionPage*> _pages;

	QMap<int, QCheckBox*> _ownerCheckBoxes;
	QMap<QCheckBox*, int> _ownerCheckBoxesR;

	QList<int> _checkedOwners;

	void ClearUI();
	void ClearUserCheckBoxUI();

	Ui::QtTransactionsClass* ui;

	void SetAllOwners();

public slots:
	void UpdateUI();
	void PageChanged();
	void ApplyFilters();
	void CreateTransactionPressed();
	void OnChooseCategories();
	void OnCreateTransactionRule();

signals:
	void UpdateTransactionsUI();

private:
	
};
