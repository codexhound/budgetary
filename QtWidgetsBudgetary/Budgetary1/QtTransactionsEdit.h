#pragma once

#include <QWidget>
#include "ui_QtTransactionsEdit.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtTransactionsEditClass; };
QT_END_NAMESPACE

class QtTransactionsEdit : public QWidget
{
	Q_OBJECT

public:
	QtTransactionsEdit(QWidget *parent = nullptr);
	~QtTransactionsEdit();

	Ui::QtTransactionsEditClass* ui;

	QMap<int, int> _catComboIndexMap; //first: selection index, second: cat ID/key
	QMap<int, int> _catComboIndexMapR;

	QMap<int, int> _accountComboIndexMap;
	QMap<int, int> _accountComboIndexMapR;

	QMap<int, int> _ownerComboIndexMap; //first selection index, second: joint, or specific user
	QMap<int, int> _ownerComboIndexMapR;

	int _transID;
	double _positive;

	void ClearUI();
	void OnSetupUI();

public slots:
	void SplitTransactionPressed();
	void DeleteTransactionPressed();
	void OkPressed();
	void CancelPressed();

private:
	
};
