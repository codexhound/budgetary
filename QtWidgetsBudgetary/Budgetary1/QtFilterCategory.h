#pragma once

#include <QWidget>
#include "ui_QtFilterCategory.h"
#include "QtCategoryCheckListChild.h"
#include "QtCategoryCheckListParent.h"
#include "CategoryData.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtFilterCategoryClass; };
QT_END_NAMESPACE

class QtFilterCategory : public QWidget
{
	Q_OBJECT

public:
	QtFilterCategory(QWidget *parent = nullptr);
	~QtFilterCategory();

	Ui::QtFilterCategoryClass* ui;

	QMap<int, QtCategoryCheckListParent* > _catWidgetMap;
	QMap<int, CategoryData*> _checkCats;

	void SetCheckedCategories(QList<int>& list);
	void CheckAll();
	void ClearUI();
	void OnSetupUI();

	bool isCatDisp(int catId);

	bool isAllChecked();

public slots:
	void OnCancel();
	void OnApply();
	void OnCheckAllChanged();

private:
	
};
