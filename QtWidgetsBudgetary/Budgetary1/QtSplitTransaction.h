#pragma once

#include <QWidget>
#include "ui_QtSplitTransaction.h"
#include "QtSplitTransactionPart.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtSplitTransactionClass; };
QT_END_NAMESPACE

class QtSplitTransaction : public QWidget
{
	Q_OBJECT

public:
	QtSplitTransaction(QWidget* parent = nullptr);
	~QtSplitTransaction();

	int _firstID;
	double _positive;

	QList<QtSplitTransactionPart*> _transactionParts;

	Ui::QtSplitTransactionClass* ui;

	void RemovePart(QtSplitTransactionPart* part);

	void ClearUI();
	void OnSetupUI(int tranID);
	void OkPressed();
	void CancelPressed();
	void AddTransactionPressed();

private:
	
};
