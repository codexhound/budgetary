#include "QtAccounts.h"
#include "QtListAccount.h"
#include <qurl.h>
#include <QtWebEngineWidgets/qwebengineview.h>
#include "QtWidgetsBudgetary.h"
#include "StaticH.h"
#include <qtimer.h>
#include <qmessagebox.h>
#include <QtNetwork/qnetworkreply.h>
#include "QtListAccount.h"

QtAccounts::QtAccounts(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtAccountsClass())
{
	ui->setupUi(this);

	_timer = new QTimer(this);

	connect(_timer, &QTimer::timeout, this, &QtAccounts::CheckLinkedAccountsReturn);
	connect(ui->_addAccountButton, &QPushButton::pressed, this, &QtAccounts::AddAccountPressed);
	connect(this, &QtAccounts::UpdateAccountsUI, this, &QtAccounts::UpdateUI);
}

QtAccounts::~QtAccounts()
{
	delete ui;
}

void QtAccounts::AddAccountPressed()
{
	StaticController::_mainWindow->_chooseAccountType->ResetUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_chooseAccountType);
}

void QtAccounts::ClearAccountsList()
{
	QMap<int, QtListAccount*>::iterator i;
	for (i = _accountsList.begin(); i != _accountsList.end(); i++)
	{
		delete i.value();
	}
	_accountsList.clear();
}

void QtAccounts::UpdateUI()
{
	StaticController::_mainWindow->SetDebugText("Updating Account UI");
	ClearAccountsList();
	AccountController* controller = StaticController::_accountsController;
	if (controller != nullptr)
	{
		AccountData* data = nullptr;
		QtListAccount* listaccount = nullptr;
		QMap<int, AccountData*>::iterator i;
		QMap<int, AccountData*>& accounts = controller->_accounts;
		QMap<int, QtListAccount*>::iterator find;
		for (i = accounts.begin(); i != accounts.end(); i++)
		{
			data = i.value();
			listaccount = new QtListAccount(this);
			listaccount->ui->_accountName->setText(data->_name);
			listaccount->ui->_balance->setText("Balance: $" + QString::number(data->_balance));
			listaccount->_ID = i.key();

			_accountsList.insert(i.key(), listaccount);
			ui->_scrollAreaWidgetContents->layout()->addWidget(listaccount);
		}
	}
}

void QtAccounts::LinkedPageLoaded()
{
	connect(StaticController::_mainWindow->_webView->page(), &QWebEnginePage::newWindowRequested, this, &QtAccounts::linkAccountNewWindowRequested);
	connect(StaticController::_mainWindow, &QtWidgetsBudgetary::CheckForLinksFinished, this, &QtAccounts::LinkAccountFinished); //get signal after javascript ui is done
	StaticController::_mainWindow->_webView->page()->runJavaScript("GetPublicPlaid();", [this](const QVariant& v) {});
	StaticController::_mainWindow->SetDebugText("Starting Timer");
	_timer->start(500); //start the timer to check if the function has returned a value
}

void QtAccounts::linkAccountNewWindowRequested(QWebEngineNewWindowRequest &request)
{
	request.openIn(StaticController::_mainWindow->_webViewWindow->page());

	//show the new window
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_webViewWindow);

	connect(StaticController::_mainWindow->_webViewWindow->page(), &QWebEnginePage::windowCloseRequested, this, &QtAccounts::onNewWindowClosed);
}

void QtAccounts::onNewWindowClosed()
{
	//hide the new window
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_webView);
}

void QtAccounts::LinkAccountFinished()
{
	StaticController::_mainWindow->SetDebugText("Link Accounts Finished");
	_timer->stop(); //stops the timer
}

void QtAccounts::CheckLinkedAccountsReturn()
{
	StaticController::_mainWindow->SetDebugText("Checking Request");
	StaticController::Get("http://WIN-Q75VOTO0D0V:80/api/getLatestLinkedAccount");
}


