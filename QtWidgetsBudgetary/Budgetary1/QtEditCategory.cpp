#include "QtEditCategory.h"
#include "StaticH.h"
#include <qjsonobject>

QtEditCategory::QtEditCategory(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtEditCategoryClass())
{
	ui->setupUi(this);

	_ID = 0;

	connect(ui->_okButton, &QPushButton::pressed, this, &QtEditCategory::OkPressed);
	connect(ui->_cancelButton, &QPushButton::pressed, this, &QtEditCategory::CancelPressed);
	connect(ui->_deleteButton, &QPushButton::pressed, this, &QtEditCategory::DeletePressed);
	
}

void QtEditCategory::DeletePressed()
{
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = _ID;
		data["isActive"] = 0;
		data["Show"] = 0;
		data["parentId"] = 0;
		data["name"] = "";
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateCategory";
		StaticController::Post(url, obj);

	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_categories);
}

void QtEditCategory::UpdateUI()
{
	ClearUI();

	auto& catMap = StaticController::_accountsController->_categories;
	auto findCat = catMap.find(_ID);
	int parentId = 0;

	ui->_nameEdit->setText("");
	ui->_ignoreCheckbox->setChecked(true);

	bool catExists = (findCat != catMap.end());
	bool isMasterCat = false;

	

	if (catExists)
	{
		ui->_nameEdit->setText(findCat.value()->_name);
		ui->_ignoreCheckbox->setChecked(!findCat.value()->_isShown);
		parentId = findCat.value()->_parentID;
		isMasterCat = findCat.value()->_masterId;
	}

	ui->_deleteButton->setEnabled(catExists && !isMasterCat);
	ui->_okButton->setEnabled(!isMasterCat);

	ui->_parentCatCombo->addItem("None");
	_catComboMap[0] = 0;
	_catComboMapR[0] = 0;
	int index = 1;

		for (auto i = catMap.begin(); i != catMap.end(); i++)
		{
			if (i.value()->_parentID <= 0) //parent category
			{
				ui->_parentCatCombo->addItem(i.value()->_name);
				_catComboMap[index] = i.key();
				_catComboMapR[i.key()] = index;
				index++;
			}
		}

	ui->_parentCatCombo->setCurrentIndex(_catComboMapR[parentId]);
}

void QtEditCategory::ClearUI()
{
	_catComboMap.clear();
	_catComboMapR.clear();
	ui->_parentCatCombo->clear();
}

void QtEditCategory::OkPressed()
{
	//look for errors in the edit
	auto& catMap = StaticController::_accountsController->_categories;
	auto findCat = catMap.find(_ID);
	bool error = false;
	bool parentError = false;
	if (findCat != catMap.end())
	{
		if (findCat.value()->_parentID <= 0 && ui->_parentCatCombo->currentIndex() > 0) //we are editing a parent and the current parent index isn't none, cannot change parent of a parent
		{
			parentError = true;
		}
	}

	QString name = ui->_nameEdit->text();
	int parentCat = _catComboMap[ui->_parentCatCombo->currentIndex()];
	int isShown = !ui->_ignoreCheckbox->isChecked();

	error = parentError;
	if (!error && name != "")
	{
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = _ID;
		data["isActive"] = 1;
		data["Show"] = isShown;
		data["parentId"] = parentCat;
		data["name"] = name;
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateCategory";
		StaticController::Post(url, obj);
	}

	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_categories);
}

void QtEditCategory::CancelPressed()
{
	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_categories);
}

QtEditCategory::~QtEditCategory()
{
	delete ui;
}
