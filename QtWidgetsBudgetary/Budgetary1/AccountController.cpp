#include "AccountController.h"


AccountController::AccountController(QObject *parent)
	: QObject(parent)
{
	UserData * user = new UserData(this);
	user->_ID = 1;
	user->_name = "Michael Bourquin";
	_users.insert(1, user);

	user = new UserData(this);
	user->_ID = 2;
	user->_name = "Kristina Bourquin";
	_users.insert(2, user);
}

bool AccountController::isIncomeCat(int catId)
{
	auto findCat = _categories.find(catId);
	if (findCat != _categories.end())
	{
		if (findCat.value()->_name == "Income")
		{
			return true;
		}
		auto findParent = _categories.find(findCat.value()->_parentID);
		if (findParent != _categories.end())
		{
			if (findParent.value()->_name == "Income")
			{
				return true;
			}
		}
	}

	return false;
}

bool AccountController::checkIfExistsInList(int key, QList<int>& list)
{
	for (auto i = 0; i < list.size(); i++)
	{
		if (key == list.at(i))
		{
			return true;
		}
	}
	return false;
}

void AccountController::GetExpenseCats(QList<int>& list)
{
	list.clear();
	QList<int> incomeCats;
	GetIncomeCats(incomeCats);
	for (auto i = _categories.begin(); i != _categories.end(); i++)
	{
		if (!checkIfExistsInList(i.key(), incomeCats)) //is not income category
		{
			list.append(i.key());
		}
	}
}

int AccountController::GetIncomeCats(QList<int>& list)
{
	list.clear();
	int incomeParent = 0;
	for (auto i = _categories.begin(); i != _categories.end(); i++)
	{
		if (i.value()->_name == "Income" && i.value()->_parentID <= 0)
		{
			incomeParent = i.key();
			break;
		}
	}
	for (auto i = _categories.begin(); i != _categories.end(); i++)
	{
		if (i.value()->_ID == incomeParent || i.value()->_parentID == incomeParent)
		{
			list.append(i.key());
		}
	}

	return incomeParent;
}

AccountController::~AccountController()
{

}

void AccountController::ClearData()
{
	for (auto i = _accounts.begin(); i != _accounts.end(); ++i)
	{
		delete i.value();
	}

	for (auto i = _categories.begin(); i != _categories.end(); ++i)
	{
		delete i.value();
	}

	for (auto i = _budgets.begin(); i != _budgets.end(); ++i)
	{
		delete i.value();
	}

	for (auto i = _transactions.begin(); i != _transactions.end(); ++i)
	{
		delete i.value();
	}

	for (auto i = _linkedBanks.begin(); i != _linkedBanks.end(); ++i)
	{
		delete i.value();
	}
	_transByDate.clear();
	_accounts.clear();
	_categories.clear();
	_budgets.clear();
	_linkedBanks.clear();
	_transactions.clear();
}
