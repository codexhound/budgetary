#include "QtPickLinkedAccountName.h"
#include <qjsonobject.h>
#include "StaticH.h"

QtPickLinkedAccountName::QtPickLinkedAccountName(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtPickLinkedAccountNameClass())
{
	_ID = 0;
	ui->setupUi(this);

	connect(ui->_okButton, &QPushButton::pressed, this, &QtPickLinkedAccountName::choseName);
}

QtPickLinkedAccountName::~QtPickLinkedAccountName()
{
	delete ui;
}

void QtPickLinkedAccountName::choseName()
{
	if (ui->_chooseName->text() != "")
	{
		QJsonObject obj;
		obj["ID"] = _ID;
		obj["name"] = ui->_chooseName->text();
		QString url = "http://WIN-Q75VOTO0D0V:80/api/setLinkName";
		StaticController::Post(url, obj);
		StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_accounts);

		//update the model and the UI
		StaticController::_mainWindow->OnDBFinishedUpdating();
	}
}


