#include "QtAccountForm.h"
#include "StaticH.h"
#include <qjsonobject.h>

QtAccountForm::QtAccountForm(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtAccountFormClass())
{
	_ID = 0;
	//ClearUI();

	ui->setupUi(this);

	connect(ui->_removeAccount, &QPushButton::pressed, this, &QtAccountForm::removeAccount);
	connect(ui->_viewTransactionsButton, &QPushButton::pressed, this, &QtAccountForm::viewTransactions);
	connect(ui->_okayPushButton, &QPushButton::pressed, this, &QtAccountForm::OkPressed);
	connect(ui->_cancelButton, &QPushButton::pressed, this, &QtAccountForm::CancelPressed);
}

QtAccountForm::~QtAccountForm()
{
	delete ui;
}

void QtAccountForm::UpdateUI()
{
	ClearUI();
	auto &accountMap = StaticController::_accountsController->_accounts;
	auto findAccount = accountMap.find(_ID);
	if (findAccount != accountMap.end()) //edit
	{
		ui->_balanceDisplay->setText("$" + QString::number(findAccount.value()->_balance));
		auto findLink = StaticController::_accountsController->_linkedBanks.find(findAccount.value()->_linkedBankID);
		if (findLink != StaticController::_accountsController->_linkedBanks.end())
		{
			ui->_linkedBankName->setText(findLink.value()->_name);
		}
		else
		{
			ui->_linkedBankName->setText("None");
		}
		ui->_nameEdit->setText(findAccount.value()->_name);
	}
}

void QtAccountForm::ClearUI()
{
	ui->_balanceDisplay->setText("$" + QString::number(0.0));
	ui->_linkedBankName->setText("None");
	ui->_nameEdit->setText("None");
}

void QtAccountForm::CancelPressed()
{
	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_accounts);
}

void QtAccountForm::removeAccount()
{
	//determine if there are any errors with the ui first

	auto& accountMap = StaticController::_accountsController->_accounts;
	auto findAccount = accountMap.find(_ID);
	bool error = false;
	if (findAccount != accountMap.end()) //edit
	{
		auto findLink = StaticController::_accountsController->_linkedBanks.find(findAccount.value()->_linkedBankID);
		if (findLink != StaticController::_accountsController->_linkedBanks.end())
		{
			error = true; //can't remove linked account, remove the link instead
		}
	}

	if (!error)
	{
		//update the server
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = _ID;
		data["isActive"] = 0;
		data["name"] = "";
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateAccount";
		StaticController::Post(url, obj);
	}

	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_accounts);
}

void QtAccountForm::viewTransactions()
{
	//todo
}

void QtAccountForm::OkPressed()
{
	//determine if there are any errors in the ui
	auto& accountMap = StaticController::_accountsController->_accounts;
	auto findAccount = accountMap.find(_ID);
	if (findAccount != accountMap.end()) //edit
	{
		auto findLink = StaticController::_accountsController->_linkedBanks.find(findAccount.value()->_linkedBankID);
		if (findLink != StaticController::_accountsController->_linkedBanks.end())
		{
			//todo
		}
	}

	QString name = ui->_nameEdit->text();
	if (name != "")
	{
		//update the server
		QJsonObject obj;
		QJsonObject data;
		obj["username"] = "mjbourquin";
		data["ID"] = _ID;
		data["isActive"] = 1;
		data["name"] = name;
		obj["Data"] = data;
		QString url = "http://WIN-Q75VOTO0D0V:80/updateAccount";
		StaticController::Post(url, obj);
	}

	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_accounts);
}
