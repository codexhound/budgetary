#pragma once

#include "QtWidgetsBudgetary.h"
#include "AccountController.h"
#include <QtNetwork/qnetworkaccessmanager.h>

class StaticController
{
public:
	static QtWidgetsBudgetary* _mainWindow;
	static AccountController* _accountsController;
	static QNetworkAccessManager* _networkManager;

	static void Post(QString url, QJsonObject data);
	static void Get(QString url);
};

