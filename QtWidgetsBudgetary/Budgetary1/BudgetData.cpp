#include "BudgetData.h"

BudgetData::BudgetData(QObject *parent)
	: QObject(parent)
{
	_ID = 0;
	_categoryID = 0;
	_name = "";
	_type = Enums::BudgetType::Monthly;
	_amount = 0.0;
	_startingDay = 0;
	_isActive = true;
}

BudgetData::~BudgetData()
{}
