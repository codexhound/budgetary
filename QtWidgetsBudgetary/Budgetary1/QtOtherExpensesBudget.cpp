#include "QtOtherExpensesBudget.h"
#include "StaticH.h"

QtOtherExpensesBudget::QtOtherExpensesBudget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtOtherExpensesBudgetClass())
{
	ui->setupUi(this);

	_catId = 0;

	connect(ui->_catNamePush, &QPushButton::pressed, this, &QtOtherExpensesBudget::ViewTransactionsPressed);
	connect(ui->_createBudget, &QPushButton::pressed, this, &QtOtherExpensesBudget::CreateBudgetPressed);
}


void QtOtherExpensesBudget::ViewTransactionsPressed()
{
	QtTransactions* transactions = StaticController::_mainWindow->_transactions;
	QtBudget* budget = StaticController::_mainWindow->_budgets;
	QtFilterCategory* catFilter = StaticController::_mainWindow->_filterTransactionCategories;

	QList<int> checkedCats;
	checkedCats.append(_catId);
	catFilter->SetCheckedCategories(checkedCats);
	QDate startDate = QDate(budget->getCurrentYearSelection(), budget->getCurrentMonthSelection(), 1);
	QDate endDate = startDate.addMonths(1);
	transactions->ui->_endDate->setDate(endDate);
	transactions->ui->_startDate->setDate(startDate);
	transactions->ui->__showIgnored->setChecked(false);
	transactions->UpdateUI();

	StaticController::_mainWindow->showWindow(transactions);
}

void QtOtherExpensesBudget::CreateBudgetPressed()
{
	QtBudgetEdit* budgetEdit = StaticController::_mainWindow->_budgetEdit;
	budgetEdit->UpdateUI();
	budgetEdit->ui->_categorySelect->setCurrentIndex(budgetEdit->_catComboMapR[_catId]);

	StaticController::_mainWindow->showWindow(budgetEdit);
}


QtOtherExpensesBudget::~QtOtherExpensesBudget()
{
	delete ui;
}
