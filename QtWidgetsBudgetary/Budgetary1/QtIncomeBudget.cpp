#include "QtIncomeBudget.h"
#include "StaticH.h"

QtIncomeBudget::QtIncomeBudget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtIncomeBudgetClass())
{
	ui->setupUi(this);

	_catId = 0;

	connect(ui->_catName, &QPushButton::pressed, this, &QtIncomeBudget::ViewTransactionsPressed);
}

void QtIncomeBudget::ViewTransactionsPressed()
{
	QList<int> incomeCats;
	/*
	QString string;
	for (int i = 0; i < incomeCats.size(); i++)
	{
		string = string + QString::number(incomeCats.at(i)) + ",";
	}
	ui->lineEdit->setText(string);*/

	QtFilterCategory* filterCategory = StaticController::_mainWindow->_filterTransactionCategories;
	QtTransactions* transactions = StaticController::_mainWindow->_transactions;
	QtBudget* budget = StaticController::_mainWindow->_budgets;

	QDate startDate = QDate(budget->getCurrentYearSelection(), budget->getCurrentMonthSelection(), 1);
	QDate endDate = startDate.addMonths(1);
	transactions->ui->_endDate->setDate(endDate);
	transactions->ui->_startDate->setDate(startDate);
	transactions->ui->__showIgnored->setChecked(true);

	incomeCats.append(_catId);
	filterCategory->SetCheckedCategories(incomeCats);
	transactions->UpdateUI();

	StaticController::_mainWindow->showWindow(transactions);
}

QtIncomeBudget::~QtIncomeBudget()
{
	delete ui;
}
