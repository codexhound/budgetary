#pragma once

#include <QWidget>
#include "ui_QtSplitTransactionPart.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtSplitTransactionPartClass; };
QT_END_NAMESPACE

class QtSplitTransactionPart : public QWidget
{
	Q_OBJECT

public:
	QtSplitTransactionPart(QWidget *parent = nullptr);
	~QtSplitTransactionPart();

	int _ID;
	double _currentAmount;

	QMap<int, int> _catComboMap;
	QMap<int, int> _catComboMapR;

	Ui::QtSplitTransactionPartClass* ui;

	void ClearUI();
	void OnSetupUI();

public slots:
	void OnAmountChanged();
	void OnRemovePressed();

private:
	
};
