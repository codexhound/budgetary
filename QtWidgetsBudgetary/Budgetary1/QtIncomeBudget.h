#pragma once

#include <QWidget>
#include "ui_QtIncomeBudget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtIncomeBudgetClass; };
QT_END_NAMESPACE

class QtIncomeBudget : public QWidget
{
	Q_OBJECT

public:
	QtIncomeBudget(QWidget *parent = nullptr);
	~QtIncomeBudget();

	Ui::QtIncomeBudgetClass* ui;

	int _catId;

public slots:
	void ViewTransactionsPressed();

private:
	
};
