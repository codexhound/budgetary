#pragma once

#include <qdatetime.h>
#include "Enums.h"
#include <QObject>

class TransactionData  : public QObject
{
	Q_OBJECT

public:
	TransactionData(QObject *parent);
	~TransactionData();

	int _ID;
	double _amount;
	QString _name;
	QString _externalName;
	QDate _date;
	QDate _authdate;
	bool _isActive;
	bool _isPending;
	Enums::Ownership _ownership;
	int _userId;
	int _catId;
	int _accountId;
};
