#include "QtTransactions.h"
#include "StaticH.h"
#include "QtTransaction.h"
#include "QtTransactionsEdit.h"

QtTransactions::QtTransactions(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtTransactionsClass())
{
	ui->setupUi(this);

	connect(this, &QtTransactions::UpdateTransactionsUI, this, &QtTransactions::UpdateUI);
	connect(ui->_pageCombo, &QComboBox::currentIndexChanged, this, &QtTransactions::PageChanged);
	connect(ui->_applyFilters, &QPushButton::pressed, this, &QtTransactions::ApplyFilters);
	connect(ui->_createTransaction, &QPushButton::pressed, this, &QtTransactions::CreateTransactionPressed);
	connect(ui->_chooseCategories, &QPushButton::pressed, this, &QtTransactions::OnChooseCategories);
	connect(ui->_transactionRule, &QPushButton::pressed, this, &QtTransactions::OnCreateTransactionRule);

	ui->_startDate->setDate(QDate::currentDate().addMonths(-1));
	ui->_endDate->setDate(QDate::currentDate());
}

QtTransactions::~QtTransactions()
{
	delete ui;
}

void QtTransactions::OnChooseCategories()
{
	StaticController::_mainWindow->_filterTransactionCategories->OnSetupUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_filterTransactionCategories);
}

void QtTransactions::CreateTransactionPressed()
{

	QtTransactionsEdit* editTrans = StaticController::_mainWindow->_editTransactions;
	editTrans->OnSetupUI();

	editTrans->ui->_deleteTranBut->setEnabled(false);
	editTrans->ui->_splitTransBut->setEnabled(false);

	StaticController::_mainWindow->showWindow(editTrans);
}

void QtTransactions::ApplyFilters()
{
	_checkedOwners.clear();
	QString value = "";
	for (auto i = _ownerCheckBoxes.begin(); i != _ownerCheckBoxes.end(); i++)
	{
		if (i.value()->isChecked())
		{
			_checkedOwners.append(i.key());
			value = value + QString::number(i.key()) + ", ";
		}
	}
	ui->lineEdit_2->setText(value);
	UpdateUI();
}

void QtTransactions::PageChanged()
{
	if (ui->_pageCombo->isEnabled())
	{
		int currentIndex = ui->_pageCombo->currentIndex();
		for (int i = 0; i < _pages.size(); i++)
		{
			_pages.at(i)->hide();
		}
		_pages.at(currentIndex)->show();
	}
}

void QtTransactions::OnCreateTransactionRule()
{
	StaticController::_mainWindow->_createTransRule->UpdateUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_createTransRule);
}

void QtTransactions::SetAllOwners()
{
	_checkedOwners.clear();
	auto& uMap = StaticController::_accountsController->_users;

	_checkedOwners.append(-1);
	for (auto i = uMap.begin(); i != uMap.end(); i++)
	{
		_checkedOwners.append(i.key());
	}
}

void QtTransactions::UpdateUI()
{
	StaticController::_mainWindow->SetDebugText("Updating Transactions UI");
	ClearUI();
	AccountController* controller = StaticController::_accountsController;
	if (controller != nullptr)
	{
		QtFilterCategory * filterCatForm = StaticController::_mainWindow->_filterTransactionCategories;
		auto& filterCats = filterCatForm->_checkCats;
		TransactionData* data = nullptr;
		QtTransaction* transaction = nullptr;
		std::map<QDate, std::map<int, TransactionData*> >& transactions = controller->_transByDate;
		QMap<int, QtTransaction*>::iterator find;
		QtTransactionPage* page = new QtTransactionPage(this);
		_pages.append(page);
		ui->_scrollAreaWidgetContents->layout()->addWidget(page);
		int tranCount = 0;
		QString ids = "";

		auto& uMap = StaticController::_accountsController->_users;
		auto findUser = uMap.begin();

		//draw checkboxes and add to data structure
		QCheckBox* checkbox = new QCheckBox();
		
		checkbox->setText("Joint");
		_ownerCheckBoxes[-1] = checkbox;
		checkbox->setChecked(StaticController::_accountsController->checkIfExistsInList(-1, _checkedOwners));

		_ownerCheckBoxesR[checkbox] = -1;
		ui->_ownerCheckboxes->layout()->addWidget(checkbox);

		int index = 1;
		for (auto i = uMap.begin(); i != uMap.end(); i++)
		{
			checkbox = new QCheckBox();
			checkbox->setChecked(StaticController::_accountsController->checkIfExistsInList(i.key(), _checkedOwners));
			checkbox->setText(i.value()->_name);
			_ownerCheckBoxes[i.key()] = checkbox;
			_ownerCheckBoxesR[checkbox] = i.key();
			ui->_ownerCheckboxes->layout()->addWidget(checkbox);
			index++;
		}

		double netAmount = 0;

		if (ui->_descRadio->isChecked())
		{
			for (auto i = std::make_reverse_iterator(transactions.upper_bound(ui->_endDate->date())); i->first >= ui->_startDate->date() && i != transactions.rend(); i++)
			{
				for (auto j = i->second.begin(); j != i->second.end(); j++)
				{
					ids = ids + QString::number(j->first) + ",";
					data = j->second;

					//find the owner checkbox;
					auto findCheckbox = _ownerCheckBoxes.find(data->_ownership);
					bool isChecked = false;
					if (findCheckbox != _ownerCheckBoxes.end())
					{
						isChecked = findCheckbox.value()->isChecked();
					}

					auto cati = controller->_categories.find(data->_catId);
					if (cati != controller->_categories.end() && isChecked)
					{
						if (((!cati.value()->_isShown && ui->__showIgnored->isChecked()) || cati.value()->_isShown) && filterCatForm->isCatDisp(data->_catId))
						{
							transaction = new QtTransaction(this);
							transaction->ui->_category->setText(cati.value()->_name);
							transaction->_ID = j->first;
							transaction->ui->_name->setText(data->_name);
							transaction->ui->_amount->setText("$" + QString::number(data->_amount));
							transaction->ui->_date->setText(data->_date.toString());
							transaction->ui->_merchantName->setText(data->_externalName);

							netAmount = netAmount + data->_amount;

							auto accounti = controller->_accounts.find(data->_accountId);
							if (accounti != controller->_accounts.end())
							{
								transaction->ui->_account->setText(accounti.value()->_name);
							}
							else
							{
								transaction->ui->_account->setText("Unknown");
							}

							page->layout()->addWidget(transaction);
							_transactions.insert(j->first, transaction);
							tranCount++;
							if (tranCount > 50) //create new page then
							{
								tranCount = 0;
								page = new QtTransactionPage(this);
								_pages.append(page);
								ui->_scrollAreaWidgetContents->layout()->addWidget(page);
							}
						}
					}
				}
			}
		}
		else
		{
			for (auto i = transactions.lower_bound(ui->_startDate->date()); i->first <= ui->_endDate->date() && i != transactions.end(); i++)
			{
				for (auto j = i->second.begin(); j != i->second.end(); j++)
				{
					data = j->second;

					//find the owner checkbox;
					auto findCheckbox = _ownerCheckBoxes.find(data->_ownership);
					bool isChecked = false;
					if (findCheckbox != _ownerCheckBoxes.end())
					{
						isChecked = findCheckbox.value()->isChecked();
					}

					auto cati = controller->_categories.find(data->_catId);
					if (cati != controller->_categories.end() && isChecked)
					{
						if (((!cati.value()->_isShown && ui->__showIgnored->isChecked()) || cati.value()->_isShown) && filterCatForm->isCatDisp(data->_catId))
						{
							transaction = new QtTransaction(this);
							transaction->ui->_category->setText(cati.value()->_name);
							transaction->_ID = j->first;
							transaction->ui->_name->setText(data->_name);
							transaction->ui->_amount->setText("$" + QString::number(data->_amount));
							transaction->ui->_date->setText(data->_date.toString());

							netAmount = netAmount + data->_amount;

							auto accounti = controller->_accounts.find(data->_accountId);
							if (accounti != controller->_accounts.end())
							{
								transaction->ui->_account->setText(accounti.value()->_name);
							}
							else
							{
								transaction->ui->_account->setText("Unknown");
							}

							page->layout()->addWidget(transaction);
							_transactions.insert(j->first, transaction);
							tranCount++;
							if (tranCount > 50) //create new page then
							{
								tranCount = 0;
								page = new QtTransactionPage(this);
								_pages.append(page);
								ui->_scrollAreaWidgetContents->layout()->addWidget(page);
							}
						}
					}
				}
			}
		}

		ui->_netAmount->setText(QString::number(netAmount));

		StaticController::_mainWindow->ui->lineEdit->setText(ids);

		for (int i = 0; i < _pages.size(); i++)
		{
			ui->_pageCombo->addItem(QString::number(i+1));
		}
		ui->_pageCombo->setCurrentIndex(0);
		PageChanged();
	}
}

void QtTransactions::ClearUserCheckBoxUI()
{
	for (auto i = _ownerCheckBoxes.begin(); i != _ownerCheckBoxes.end(); i++)
	{
		delete i.value();
	}
	_ownerCheckBoxes.clear();
	_ownerCheckBoxesR.clear();
}

void QtTransactions::ClearUI()
{
	ClearUserCheckBoxUI();
	for (auto i = _transactions.begin(); i != _transactions.end(); i++)
	{
		delete i.value();
	}
	for (int i = 0; i < _pages.size(); i++)
	{
		delete _pages.at(i);
	}
	_transactions.clear();
	_pages.clear();
	ui->_pageCombo->setDisabled(true);
	ui->_pageCombo->clear();
	ui->_pageCombo->setDisabled(false);
}
