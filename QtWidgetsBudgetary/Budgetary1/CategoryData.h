#pragma once

#include <QObject>

class CategoryData  : public QObject
{
	Q_OBJECT

public:
	CategoryData(QObject *parent);
	~CategoryData();

	int _ID;
	int _parentID;
	QString _name;
	bool _isActive;
	bool _isShown;
	int _masterId;
};
