#pragma once

#include <QWidget>
#include "ui_QtAccounts.h"
#include "QtListAccount.h"
#include "QtPickLinkedAccountName.h"
#include <QWebEngineNewWindowRequest>

QT_BEGIN_NAMESPACE
namespace Ui { class QtAccountsClass; };
QT_END_NAMESPACE

class QtAccounts : public QWidget
{
	Q_OBJECT

public:
	QtAccounts(QWidget *parent = nullptr);
	~QtAccounts();

	QTimer* _timer;

	void LinkAccountFinished();
	void ClearAccountsList();


	

public slots:
	void AddAccountPressed();
	void LinkedPageLoaded();
	void CheckLinkedAccountsReturn();
	void UpdateUI();
	void linkAccountNewWindowRequested(QWebEngineNewWindowRequest &request);
	void onNewWindowClosed();

signals:
	void UpdateAccountsUI();

private:
	Ui::QtAccountsClass *ui;

	QString m_scriptResult;

	QMap<int, QtListAccount*> _accountsList;
};
