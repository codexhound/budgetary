#pragma once

#include <QWidget>
#include "ui_QtLinkedBankForm.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtLinkedBankFormClass; };
QT_END_NAMESPACE

class QtLinkedBankForm : public QWidget
{
	Q_OBJECT

public:
	QtLinkedBankForm(QWidget *parent = nullptr);
	~QtLinkedBankForm();

private:
	Ui::QtLinkedBankFormClass *ui;
};
