#include "QtChildCatList.h"
#include "StaticH.h"

QtChildCatList::QtChildCatList(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtChildCatListClass())
{
	ui->setupUi(this);

	_ID = 0;

	connect(ui->_catEdit, &QPushButton::pressed, this, &QtChildCatList::EditPressed);
}

void QtChildCatList::EditPressed()
{
	QtEditCategory* editCat = StaticController::_mainWindow->_editCategory;
	editCat->_ID = _ID;
	editCat->UpdateUI();
	StaticController::_mainWindow->showWindow(editCat);
}

QtChildCatList::~QtChildCatList()
{
	delete ui;
}
