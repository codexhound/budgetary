#include "QtChooseAccountTypeCreate.h"
#include "StaticH.h"

QtChooseAccountTypeCreate::QtChooseAccountTypeCreate(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtChooseAccountTypeCreateClass())
{
	ui->setupUi(this);

	ResetUI();

	connect(ui->_okButton, &QPushButton::pressed, this, &QtChooseAccountTypeCreate::OnApplyPressed);
	connect(ui->_cancelButton, &QPushButton::pressed, this, &QtChooseAccountTypeCreate::OnCancelPressed);
	connect(ui->_manualRadio, &QRadioButton::toggled, this, &QtChooseAccountTypeCreate::OnTypeToggled);
	
}

void QtChooseAccountTypeCreate::OnTypeToggled()
{
	//ui->_manualBankType->setEnabled(ui->_manualRadio->isChecked());
}

void QtChooseAccountTypeCreate::ResetUI()
{
	ui->_manualRadio->setChecked(true);
	ui->_depositType->setChecked(true);
	ui->_manualBankType->setEnabled(false);
}

void QtChooseAccountTypeCreate::OnApplyPressed()
{
	QtWidgetsBudgetary* mainWindow = StaticController::_mainWindow;
	QWebEngineView* webView = mainWindow->_webView;
	QtAccounts* accounts = mainWindow->_accounts;

	if (ui->_bankLinkRadio->isChecked()) //then open up the linker PLAID API
	{
		if (webView != NULL)
		{
			accounts->connect(webView, &QWebEngineView::loadFinished, accounts, &QtAccounts::LinkedPageLoaded); //get signal after page is loaded initially
			webView->load(QUrl("http://WIN-Q75VOTO0D0V/webAPI/"));
			mainWindow->showWindow(webView);
		}
	}
	else
	{
		QtAccountForm* accountEdit = StaticController::_mainWindow->_accountForm;
		accountEdit->_ID = 0;
		//then open up the manual account edit screen

		accountEdit->UpdateUI();
		mainWindow->showWindow(accountEdit);
	}

	ResetUI();
}

void QtChooseAccountTypeCreate::OnCancelPressed()
{
	ResetUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_accounts);
}

QtChooseAccountTypeCreate::~QtChooseAccountTypeCreate()
{
	delete ui;
}
