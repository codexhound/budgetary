#pragma once

#include <QWidget>
#include "ui_QtListCategories.h"
#include "QtParentCategoryList.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtListCategoriesClass; };
QT_END_NAMESPACE

class QtListCategories : public QWidget
{
	Q_OBJECT

public:
	QtListCategories(QWidget *parent = nullptr);
	~QtListCategories();

	Ui::QtListCategoriesClass* ui;

	QMap<int, QtParentCategoryList*> _categoryList;

	void ClearUI();

public slots:
	void UpdateUI();
	void CreateCategoryPressed();

signals:
	void UpdateCategoriesUI();

private:
	
};
