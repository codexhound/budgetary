#pragma once

#include <QWidget>
#include "ui_QtCreateTransactionRule.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtCreateTransactionRuleClass; };
QT_END_NAMESPACE

class QtCreateTransactionRule : public QWidget
{
	Q_OBJECT

public:
	QtCreateTransactionRule(QWidget *parent = nullptr);
	~QtCreateTransactionRule();

	Ui::QtCreateTransactionRuleClass* ui;

	QMap<int, int> _catComboMap;
	QMap<int, int> _catComboMapR;

public:
	void UpdateUI();
	void ClearUI();

public slots:
	void OnOkPressed();
	void OnCancelPressed();

private:
	
};
