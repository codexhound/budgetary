#include "QtSplitTransactionPart.h"
#include "QtSplitTransaction.h"
#include "StaticH.h"

QtSplitTransactionPart::QtSplitTransactionPart(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtSplitTransactionPartClass())
{
	ui->setupUi(this);

	ui->_amountEdit->setKeyboardTracking(false);

	_ID = 0;
	_currentAmount = 0;

	connect(ui->_removeBut, &QPushButton::pressed, this, &QtSplitTransactionPart::OnRemovePressed);
	connect(ui->_amountEdit, &QDoubleSpinBox::valueChanged, this, &QtSplitTransactionPart::OnAmountChanged);
	connect(ui->_amountEdit, &QDoubleSpinBox::editingFinished, this, &QtSplitTransactionPart::OnAmountChanged);

}

void QtSplitTransactionPart::ClearUI()
{
	_catComboMap.clear();
	_catComboMapR.clear();
	ui->_catCombo->clear();
}
void QtSplitTransactionPart::OnSetupUI()
{
	QtSplitTransaction* parent = StaticController::_mainWindow->_splitTransactionForm;
	int transId = parent->_firstID;
	auto& tranMap = StaticController::_accountsController->_transactions;
	auto& catMap = StaticController::_accountsController->_categories;
	CategoryData* dCat = nullptr;

	int index = 0;
	for (auto it = catMap.begin(); it != catMap.end(); it++)
	{

		if (it.value()->_parentID <= 0)
		{
			//parent

			ui->_catCombo->addItem(it.value()->_name);
			_catComboMap.insert(index, it.key());
			_catComboMapR.insert(it.key(), index);

			index++;

			for (auto j = catMap.begin(); j != catMap.end(); j++)
			{
				if (j.value()->_parentID == it.key())
				{
					ui->_catCombo->addItem(it.value()->_name + "---->" + j.value()->_name);
					_catComboMap.insert(index, j.key());
					_catComboMapR.insert(j.key(), index);

					index++;
				}
			}
		}
	}

	auto findt = tranMap.find(transId);
	TransactionData* tData = nullptr;
	if (findt != tranMap.end())
	{
		tData = findt.value();
		ui->_catCombo->setCurrentIndex(_catComboMapR[tData->_catId]);
	}
}

void QtSplitTransactionPart::OnAmountChanged()
{

	QtSplitTransaction* transP = StaticController::_mainWindow->_splitTransactionForm;
	QtSplitTransactionPart* firstPart = transP->_transactionParts.at(0);
	if (ui->_amountEdit->isEnabled() && ui->_amountEdit->value() != _currentAmount)
	{
		if (ui->_amountEdit->value() < 0)
		{
			ui->_amountEdit->setValue(_currentAmount);
			return; //done, this is not a valid change
		}
		else
		{
			if (firstPart != this)
			{
				double change = ui->_amountEdit->value() - _currentAmount;
				double tempFirstPartV = firstPart->_currentAmount - change;
				ui->debug->setText(QString::number(_catComboMap[ui->_catCombo->currentIndex()]));
				if (tempFirstPartV <= 0)
				{
					ui->_amountEdit->setValue(_currentAmount);
					return; //done, this is not a valid change, the first part is equal to or less than 0
				}
				_currentAmount = ui->_amountEdit->value();
				firstPart->ui->_amountEdit->setValue(tempFirstPartV); //edit the first parts value
				firstPart->_currentAmount = tempFirstPartV;
			}
		}
	}
}
void QtSplitTransactionPart::OnRemovePressed()
{
	QtSplitTransaction* transP = StaticController::_mainWindow->_splitTransactionForm;

	//add the amount back to the parent part
	QtSplitTransactionPart* firstPart = transP->_transactionParts.at(0);
	firstPart->ui->_amountEdit->setValue(firstPart->_currentAmount + _currentAmount);

	//remove the part
	transP->RemovePart(this);
	this->deleteLater();
}

QtSplitTransactionPart::~QtSplitTransactionPart()
{
	delete ui;
}
