#include "QtWebView.h"

QtWebView::QtWebView(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtWebViewClass())
{
	ui->setupUi(this);

	_webView = new QtWebView(this);

	ui->verticalLayout->addWidget(_webView);
}

QtWebView::~QtWebView()
{
	delete ui;
}
