#pragma once

#include <QWidget>
#include "ui_QtCategoryCheckListParent.h"
#include "QtCategoryCheckListChild.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtCategoryCheckListParentClass; };
QT_END_NAMESPACE

class QtCategoryCheckListParent : public QWidget
{
	Q_OBJECT

public:
	QtCategoryCheckListParent(QWidget *parent = nullptr);
	~QtCategoryCheckListParent();

	Ui::QtCategoryCheckListParentClass* ui;

	int _ID;

	QMap<int, QtCategoryCheckListChild*> _childMap;

public slots:
	void OnCheckBoxChanged();

private:
	
};
