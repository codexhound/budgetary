#pragma once

#include <QObject>

class UserData  : public QObject
{
	Q_OBJECT

public:
	UserData(QObject *parent);
	~UserData();

	int _ID;
	QString _name;
};
