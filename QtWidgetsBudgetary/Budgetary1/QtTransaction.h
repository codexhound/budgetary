#pragma once

#include <QWidget>
#include "ui_QtTransaction.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtTransactionClass; };
QT_END_NAMESPACE

class QtTransaction : public QWidget
{
	Q_OBJECT

public:
	QtTransaction(QWidget *parent = nullptr);
	~QtTransaction();

	int _ID;

	Ui::QtTransactionClass* ui;

public slots:
	void EditPressed();
	
};
