#include "QtBudgetList.h"
#include "StaticH.h"
#include "QtBudgetEdit.h"

QtBudgetList::QtBudgetList(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtBudgetListClass())
{
	ui->setupUi(this);

	connect(ui->_viewEdit, &QPushButton::pressed, this, &QtBudgetList::OnViewEditPressed);
	connect(ui->_viewTransactions, &QPushButton::pressed, this, &QtBudgetList::OnViewTransactionsPressed);
}

void QtBudgetList::OnViewEditPressed()
{
	QtBudgetEdit* budgetEdit = StaticController::_mainWindow->_budgetEdit;
	auto& budgetMap = StaticController::_accountsController->_budgets;
	auto findBudget = budgetMap.find(_ID);
	if (findBudget != budgetMap.end())
	{
		QString name = findBudget.value()->_name;
		double amount = findBudget.value()->_amount;
		int catId = findBudget.value()->_categoryID;

		budgetEdit->UpdateUI();
		budgetEdit->_ID = _ID;
		budgetEdit->ui->_categorySelect->setCurrentIndex(budgetEdit->_catComboMapR[catId]);
		budgetEdit->ui->_amountSpinner->setValue(amount);
		budgetEdit->ui->_nameEdit->setText(name);

		StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_budgetEdit);
	}
}

void QtBudgetList::OnViewTransactionsPressed()
{
	auto &tranMap = StaticController::_accountsController->_transactions;
	QtTransactions* transactions = StaticController::_mainWindow->_transactions;

	//update transactions UI
	StaticController::_mainWindow->_filterTransactionCategories->SetCheckedCategories(_catList);
	transactions->ui->_startDate->setDate(_startDate);
	transactions->ui->_endDate->setDate(_endDate);
	transactions->ui->__showIgnored->setChecked(false);
	transactions->UpdateUI();

	//then show transactions
	StaticController::_mainWindow->showWindow(transactions);
}

QtBudgetList::~QtBudgetList()
{
	delete ui;
}
