#include "CategoryData.h"

CategoryData::CategoryData(QObject *parent)
	: QObject(parent)
{
	_ID = 0;
	_parentID = 0;
	_name = "";
	_isActive = true;
	_isShown = true;
	_masterId = 0;
}

CategoryData::~CategoryData()
{}
