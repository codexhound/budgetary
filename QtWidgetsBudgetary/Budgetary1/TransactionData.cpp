#include "TransactionData.h"

TransactionData::TransactionData(QObject *parent)
	: QObject(parent)
{
	_ID = 0;
	_amount = 0.0;
	_name = "";
	_externalName = "";
	_isActive = true;
	_isPending = false;
	_ownership = Enums::Ownership::joint;
	_userId = 1;
	_catId = 0;
	_accountId = 0;
}

TransactionData::~TransactionData()
{}
