#include "QtCreateTransactionRule.h"
#include "StaticH.h"
#include "qjsonobject.h"

QtCreateTransactionRule::QtCreateTransactionRule(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtCreateTransactionRuleClass())
{
	ui->setupUi(this);

	ui->_logicCombo->addItem("LIKE");

	connect(ui->_okButton, &QPushButton::pressed, this, &QtCreateTransactionRule::OnOkPressed);
	connect(ui->_cancelButton, &QPushButton::pressed, this, &QtCreateTransactionRule::OnCancelPressed);
}

void QtCreateTransactionRule::UpdateUI()
{
	int index = 0;
	ClearUI();
	auto& catMap = StaticController::_accountsController->_categories;
	for (auto it = catMap.begin(); it != catMap.end(); it++)
	{
		if (it.value()->_parentID <= 0)
		{
			//parent

			ui->_catCombo->addItem(it.value()->_name);
			_catComboMap.insert(index, it.key());
			_catComboMapR.insert(it.key(), index);

			index++;

			for (auto j = catMap.begin(); j != catMap.end(); j++)
			{
				if (j.value()->_parentID == it.key())
				{
					ui->_catCombo->addItem(it.value()->_name + "---->" + j.value()->_name);
					_catComboMap.insert(index, j.key());
					_catComboMapR.insert(j.key(), index);

					index++;
				}
			}
		}
	}
	ui->_catCombo->setCurrentIndex(0);
}
void QtCreateTransactionRule::ClearUI()
{
	_catComboMap.clear();
	_catComboMapR.clear();
	ui->_catCombo->clear();
}

void QtCreateTransactionRule::OnOkPressed()
{
	//post the data from this page to the server
	QJsonObject obj;
	QJsonObject data;
	obj["username"] = "mjbourquin";
	data["Name"] = ui->_nameEdit->text();
	data["Type"] = ui->_logicCombo->currentIndex();
	data["CatID"] = _catComboMap[ui->_catCombo->currentIndex()];
	data["NameTo"] = ui->_nameTo->text();
	obj["Data"] = data;
	QString url = "http://WIN-Q75VOTO0D0V:80/updateTransactionRule";
	StaticController::Post(url, obj);

	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}
void QtCreateTransactionRule::OnCancelPressed()
{
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}

QtCreateTransactionRule::~QtCreateTransactionRule()
{
	delete ui;
}
