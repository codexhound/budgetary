#include "QtFilterCategory.h"
#include "StaticH.h"

QtFilterCategory::QtFilterCategory(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtFilterCategoryClass())
{
	ui->setupUi(this);

	connect(ui->_okBut , &QPushButton::pressed, this, &QtFilterCategory::OnApply);
	connect(ui->_cancelBut, &QPushButton::pressed, this, &QtFilterCategory::OnCancel);
	connect(ui->_checkAll, &QCheckBox::stateChanged, this, &QtFilterCategory::OnCheckAllChanged);
}

void QtFilterCategory::ClearUI()
{
	for (auto it = _catWidgetMap.begin(); it != _catWidgetMap.end(); it++)
	{
		delete it.value(); //delete the parent checkbox widget
	}
	_catWidgetMap.clear();
}

bool QtFilterCategory::isCatDisp(int catId)
{
	auto findId = _checkCats.find(catId);
	if (findId != _checkCats.end())
	{
		return true;
	}
	else return false;
}

bool QtFilterCategory::isAllChecked()
{
	for (auto it = _catWidgetMap.begin(); it != _catWidgetMap.end(); it++)
	{
		if (!it.value()->ui->_parentCheckBox->isChecked())
		{
			return false;
		}
		for(auto j = it.value()->_childMap.begin(); j != it.value()->_childMap.end(); j++)
		{ 
			if (!j.value()->ui->_checkBox->isChecked())
			{
				return false;
			}
		}
	}
	return true;
}

void QtFilterCategory::OnCheckAllChanged()
{
	if (ui->_checkAll->isEnabled())
	{
		bool isChecked = ui->_checkAll->isChecked();
		for (auto i = _catWidgetMap.begin(); i != _catWidgetMap.end(); i++)
		{
			i.value()->ui->_parentCheckBox->setChecked(isChecked);
		}
	}
}

void QtFilterCategory::CheckAll()
{
	QList<int> list;
	auto& catMap = StaticController::_accountsController->_categories;
	for (auto it = catMap.begin(); it != catMap.end(); it++)
	{
		list.append(it.key());
	}
	SetCheckedCategories(list);
}

void QtFilterCategory::SetCheckedCategories(QList<int>& list)
{
	_checkCats.clear();
	auto& catMap = StaticController::_accountsController->_categories;
	for (int i = 0; i < list.size(); i++)
	{
		auto findit = catMap.find(list.at(i));
		if (findit != catMap.end())
		{
			_checkCats[list.at(i)] = findit.value();
		}
	}
	OnSetupUI();
}

void QtFilterCategory::OnCancel()
{
	//restore the previous ui data
	OnSetupUI();
	
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}
void
QtFilterCategory::OnApply()
{
	//update the checked categories from the UI and then update the transactions UI
	_checkCats.clear();
	auto& catMap = StaticController::_accountsController->_categories;
	auto catFind = catMap.begin();
	for (auto it = _catWidgetMap.begin(); it != _catWidgetMap.end(); it++)
	{
		catFind = catMap.find(it.key());
		if (catFind != catMap.end())
		{
			if (it.value()->ui->_parentCheckBox->isChecked())
			{
				_checkCats[it.key()] = catFind.value();
			}
			
			for (auto j = it.value()->_childMap.begin(); j != it.value()->_childMap.end(); j++)
			{
				catFind = catMap.find(j.key());
				if (catFind != catMap.end())
				{
					if (j.value()->ui->_checkBox->isChecked())
					{
						_checkCats[j.key()] = catFind.value();
					}
				}
			}
		}
	}

	StaticController::_mainWindow->_transactions->UpdateUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}


void QtFilterCategory::OnSetupUI()
{
	ClearUI();

	QtCategoryCheckListParent* catCheckParent = nullptr;
	QtCategoryCheckListChild* catCheckChild = nullptr;

	auto& catMap = StaticController::_accountsController->_categories;
	CategoryData* catD = nullptr;

	//create parent widgets and add to map
	bool isChecked;

	QGridLayout* layout = dynamic_cast<QGridLayout*>(ui->_widgets->layout());
	int row = 0;
	int column = 0;
	if (layout != nullptr)
	{
		for (auto it = catMap.begin(); it != catMap.end(); it++)
		{
			catD = it.value();
			if (catD->_parentID <= 0) //parent
			{
				catCheckParent = new QtCategoryCheckListParent();
				auto listFind = _checkCats.find(it.key());
				if (listFind != _checkCats.end()) //found
				{
					isChecked = true;
				}
				else isChecked = false;
				catCheckParent->ui->_parentCheckBox->setChecked(isChecked);
				catCheckParent->ui->_parentCheckBox->setText(catD->_name);

				_catWidgetMap.insert(catD->_ID, catCheckParent);
				layout->addWidget(catCheckParent, row, column);
				column++;
				if (column > 2)
				{
					row++;
					column = 0;
				}
			}
		}

		//create child widgets and add to map
		for (auto it = catMap.begin(); it != catMap.end(); it++)
		{
			catD = it.value();
			if (catD->_parentID > 0) //child
			{
				auto findParent = _catWidgetMap.find(catD->_parentID);
				if (findParent != _catWidgetMap.end())
				{
					catCheckParent = findParent.value();
					if (catCheckParent != nullptr)
					{
						catCheckChild = new QtCategoryCheckListChild();
						auto listFind = _checkCats.find(it.key());
						if (listFind != _checkCats.end()) //found
						{
							isChecked = true;
						}
						else isChecked = false;
						catCheckChild->ui->_checkBox->setChecked(isChecked);
						catCheckChild->ui->_checkBox->setText(catD->_name);

						catCheckParent->_childMap[catD->_ID] = catCheckChild;
						catCheckParent->ui->_childWidgets->layout()->addWidget(catCheckChild);
					}
				}
			}
		}
	}

	if (isAllChecked())
	{
		ui->_checkAll->setChecked(true);
	}
	
}
	

QtFilterCategory::~QtFilterCategory()
{
	delete ui;
}
