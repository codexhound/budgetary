#include "QtLinkedBanks.h"
#include "StaticH.h"

QtLinkedBanks::QtLinkedBanks(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtLinkedBanksClass())
{
	ui->setupUi(this);

	connect(this, &QtLinkedBanks::UpdateLinkedBankUI, this, &QtLinkedBanks::UpdateUI);
	connect(ui->_addBankButton, &QPushButton::pressed, this, &QtLinkedBanks::AddBankPressed);
}

void QtLinkedBanks::AddBankPressed()
{
	QtWidgetsBudgetary* mainWindow = StaticController::_mainWindow;
	QWebEngineView* webView = mainWindow->_webView;
	QtAccounts* accounts = mainWindow->_accounts;

	if (webView != nullptr)
		{
			accounts->connect(webView, &QWebEngineView::loadFinished, accounts, &QtAccounts::LinkedPageLoaded); //get signal after page is loaded initially
			webView->load(QUrl("http://WIN-Q75VOTO0D0V/webAPI/"));
			mainWindow->showWindow(webView);
		}
}

QtLinkedBanks::~QtLinkedBanks()
{
	delete ui;
}

void QtLinkedBanks::ClearUI()
{
	for (auto i = _linkedBanks.begin(); i != _linkedBanks.end(); i++)
	{
		delete i.value();
	}
	_linkedBanks.clear();
}

void QtLinkedBanks::UpdateUI()
{
	StaticController::_mainWindow->SetDebugText("Updating Linked Banks UI");
	ClearUI();
	AccountController* controller = StaticController::_accountsController;
	if (controller != nullptr)
	{
		LinkedBankData* data = nullptr;
		QtLinkedBank* linkedBank = nullptr;
		QMap<int, LinkedBankData*>::iterator i;
		QMap<int, LinkedBankData*>& linkedBanks = controller->_linkedBanks;
		QMap<int, QtLinkedBank*>::iterator find;
		for (i = linkedBanks.begin(); i != linkedBanks.end(); i++)
		{
			data = i.value();
			linkedBank = new QtLinkedBank(this);
			linkedBank->_ID = i.key();
			linkedBank->ui->_name->setText(data->_name);

			_linkedBanks.insert(i.key(), linkedBank);
			ui->scrollAreaWidgetContents->layout()->addWidget(linkedBank);
		}
	}
}
