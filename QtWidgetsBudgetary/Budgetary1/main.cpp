#include "QtWidgetsBudgetary.h"
#include <QtWidgets/QApplication>
#include "StaticH.h"
#include <iostream>

int main(int argc, char *argv[])
{
    //QtWebView::initialize();
    QApplication a(argc, argv);
    StaticController::_mainWindow = new QtWidgetsBudgetary();
    //QtWidgetsBudgetary w;
    StaticController::_mainWindow->show();
    return a.exec();
}
