#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Budgetary1.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Budgetary1Class; };
QT_END_NAMESPACE

class Budgetary1 : public QMainWindow
{
    Q_OBJECT

public:
    Budgetary1(QWidget *parent = nullptr);
    ~Budgetary1();

private:
    Ui::Budgetary1Class *ui;
};
