#pragma once

#include <QWidget>
#include "ui_QtChooseAccountTypeCreate.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtChooseAccountTypeCreateClass; };
QT_END_NAMESPACE

class QtChooseAccountTypeCreate : public QWidget
{
	Q_OBJECT

public:
	QtChooseAccountTypeCreate(QWidget *parent = nullptr);
	~QtChooseAccountTypeCreate();

	void ResetUI();

public slots:
	void OnApplyPressed();
	void OnCancelPressed();
	void OnTypeToggled();

private:
	Ui::QtChooseAccountTypeCreateClass *ui;
};
