#include "QtHouseholdMember.h"
#include "StaticH.h"

QtHouseholdMember::QtHouseholdMember(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::QtHouseholdMemberClass())
{
	ui->setupUi(this);

	connect(ui->_memberName, &QPushButton::pressed, this, &QtHouseholdMember::MemberButtonPressed);

	_ID = -1;
}

void QtHouseholdMember::MemberButtonPressed()
{
	QtTransactions* transactions = StaticController::_mainWindow->_transactions;
	QtBudget* budget = StaticController::_mainWindow->_budgets;
	auto& tMap = StaticController::_accountsController->_transactions;
	QtFilterCategory* filterCategory = StaticController::_mainWindow->_filterTransactionCategories;

	QList<int> expenseCats;
	StaticController::_accountsController->GetExpenseCats(expenseCats);
	filterCategory->SetCheckedCategories(expenseCats);

	transactions->_checkedOwners.clear();
	transactions->_checkedOwners.append(_ID);

	QDate startDate = QDate(budget->getCurrentYearSelection(), budget->getCurrentMonthSelection(), 1);
	QDate endDate = startDate.addMonths(1);
	transactions->ui->_endDate->setDate(endDate);
	transactions->ui->_startDate->setDate(startDate);
	transactions->ui->__showIgnored->setChecked(false);
	transactions->UpdateUI();

	StaticController::_mainWindow->showWindow(transactions);
}

QtHouseholdMember::~QtHouseholdMember()
{
	delete ui;
}
