#pragma once

#include <QWidget>
#include "ui_QtBudgetEdit.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtBudgetEditClass; };
QT_END_NAMESPACE

class QtBudgetEdit : public QWidget
{
	Q_OBJECT

public:
	QtBudgetEdit(QWidget *parent = nullptr);
	~QtBudgetEdit();

	int _ID;

	QMap<int, int> _catComboMap;
	QMap<int, int> _catComboMapR;

	void ClearUI();
	void UpdateUI();

	Ui::QtBudgetEditClass* ui;

public slots:
	void OkPressed();
	void CancelPressed();
	void DeletePressed();

private:
	
};
