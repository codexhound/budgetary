#include "StaticH.h"
#include <qjsonobject.h>
#include <qjsondocument.h>

QtWidgetsBudgetary* StaticController::_mainWindow = NULL;
AccountController* StaticController::_accountsController = NULL;
QNetworkAccessManager* StaticController::_networkManager = NULL;

void StaticController::Post(QString url, QJsonObject data)
{
	QJsonDocument doc(data);
	QByteArray byted = doc.toJson();
	_networkManager->post(QNetworkRequest(QUrl(url)), byted);
}

void StaticController::Get(QString url)
{
	_networkManager->get(QNetworkRequest(QUrl(url)));
}
