#pragma once

#include <QWidget>
#include "ui_QtListAccount.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtListAccountClass; };
QT_END_NAMESPACE

class QtListAccount : public QWidget
{
	Q_OBJECT

public:
	QtListAccount(QWidget *parent = nullptr);
	~QtListAccount();

	int _ID;

	Ui::QtListAccountClass* ui;
public slots:
	void ViewEditPressed();
	void DeletePressed();
	
};
