#include "QtSplitTransaction.h"
#include "StaticH.h"
#include <qjsonobject.h>
#include <qjsonarray.h>

QtSplitTransaction::QtSplitTransaction(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::QtSplitTransactionClass())
{
	ui->setupUi(this);

	_positive = 1;
	_firstID = 0;

	connect(ui->_addTransaction, &QPushButton::pressed, this, &QtSplitTransaction::AddTransactionPressed);
	connect(ui->_okBut, &QPushButton::pressed, this, &QtSplitTransaction::OkPressed);
	connect(ui->_cancelBut, &QPushButton::pressed, this, &QtSplitTransaction::CancelPressed);

}

void QtSplitTransaction::OkPressed()
{
	//gather the transactions and send them off
	//must process this
	auto& transMap = StaticController::_accountsController->_transactions;
	auto findit = transMap.find(_firstID);
	TransactionData* tdata = nullptr;
	QtSplitTransactionPart* part = nullptr;
	bool valid = true;
	if (findit != transMap.end())
	{
		tdata = findit.value();
		//check that all parts are valid
		
		double amount = 0;
		QString name = "";
		for (int i = 0; i < _transactionParts.size(); i++)
		{
			part = _transactionParts.at(i);
			amount = part->ui->_amountEdit->value();
			name = part->ui->_nameEdit->text();
			if (amount <= 0 || name == "")
			{
				valid = false;
				break;
			}
		}

		if (valid)
		{
			QJsonObject obj;
			QJsonArray data;
			obj["username"] = "mjbourquin";

			for (int i = 0; i < _transactionParts.size(); i++)
			{
				part = _transactionParts.at(i);

				amount = part->ui->_amountEdit->value();
				name = part->ui->_nameEdit->text();
				int catId = part->_catComboMap[part->ui->_catCombo->currentIndex()];

				QJsonObject pData;
				pData["ID"] = part->_ID;
				pData["isActive"] = 1;
				pData["accountId"] = tdata->_accountId;
				pData["catID"] = catId;
				pData["owner"] = tdata->_ownership;
				pData["amount"] = amount*_positive;
				pData["date"] = tdata->_date.toString(Qt::ISODate);
				pData["name"] = name;
				data.append(pData);
			}
			obj["Data"] = data;
			QString url = "http://WIN-Q75VOTO0D0V:80/updateMultiTransaction";
			StaticController::Post(url, obj);
		}
	}
	else
	{
		valid = false;
	}

	if (!valid)
	{
		//show error dialog
	}

	CancelPressed();
}

void QtSplitTransaction::CancelPressed()
{
	ClearUI();
	StaticController::_mainWindow->showWindow(StaticController::_mainWindow->_transactions);
}

void QtSplitTransaction::AddTransactionPressed()
{
	QtSplitTransactionPart* part = new QtSplitTransactionPart(this);
	part->OnSetupUI();
	ui->_transactions->layout()->addWidget(part);
	_transactionParts.append(part);
}

void QtSplitTransaction::ClearUI()
{
	_firstID = 0;
	for (auto i = 0; i < _transactionParts.size(); i++)
	{
		delete _transactionParts.at(i);
	}
	_transactionParts.clear();
}
void QtSplitTransaction::OnSetupUI(int tranId)
{
	ClearUI();
	_firstID = tranId;
	auto& tranMap = StaticController::_accountsController->_transactions;
	auto findId = tranMap.find(tranId);
	QtSplitTransactionPart* part = nullptr;
	TransactionData* data = nullptr;
	if (findId != tranMap.end())
	{
		data = findId.value();
		_positive = 1.0;
		if (data->_amount < 0)
		{
			_positive = -1.0;
		}
		part = new QtSplitTransactionPart(this);
		ui->_transactions->layout()->addWidget(part);
		_transactionParts.append(part);
		part->ui->_removeBut->setEnabled(false);
		part->ui->_nameEdit->setText(data->_name);
		part->ui->_amountEdit->setValue(data->_amount*_positive);
		part->_currentAmount = data->_amount*_positive;
		part->ui->_amountEdit->setEnabled(false);
		part->_ID = data->_ID;
		part->OnSetupUI();
	}
}

void QtSplitTransaction::RemovePart(QtSplitTransactionPart* part)
{
	for (int i = 0; i < _transactionParts.size(); i++)
	{
		if (part == _transactionParts.at(i))
		{
			_transactionParts.removeAt(i);
			break;
		}
	}
}

QtSplitTransaction::~QtSplitTransaction()
{
	delete ui;
}
