# source /Users/tnappy/node_projects/quickstart/python/bin/activate
# Read env vars from .env file
from plaid.exceptions import ApiException
from plaid.model.payment_amount import PaymentAmount
from plaid.model.payment_amount_currency import PaymentAmountCurrency
from plaid.model.products import Products
from plaid.model.country_code import CountryCode
from plaid.model.recipient_bacs_nullable import RecipientBACSNullable
from plaid.model.payment_initiation_address import PaymentInitiationAddress
from plaid.model.payment_initiation_recipient_create_request import PaymentInitiationRecipientCreateRequest
from plaid.model.payment_initiation_payment_create_request import PaymentInitiationPaymentCreateRequest
from plaid.model.payment_initiation_payment_get_request import PaymentInitiationPaymentGetRequest
from plaid.model.link_token_create_request_payment_initiation import LinkTokenCreateRequestPaymentInitiation
from plaid.model.item_public_token_exchange_request import ItemPublicTokenExchangeRequest
from plaid.model.link_token_create_request import LinkTokenCreateRequest
from plaid.model.link_token_create_request_user import LinkTokenCreateRequestUser
from plaid.model.asset_report_create_request import AssetReportCreateRequest
from plaid.model.asset_report_create_request_options import AssetReportCreateRequestOptions
from plaid.model.asset_report_user import AssetReportUser
from plaid.model.asset_report_get_request import AssetReportGetRequest
from plaid.model.asset_report_pdf_get_request import AssetReportPDFGetRequest
from plaid.model.auth_get_request import AuthGetRequest
from plaid.model.transactions_get_request import TransactionsGetRequest
from plaid.model.transactions_get_request_options import TransactionsGetRequestOptions
from plaid.model.identity_get_request import IdentityGetRequest
from plaid.model.investments_transactions_get_request_options import InvestmentsTransactionsGetRequestOptions
from plaid.model.investments_transactions_get_request import InvestmentsTransactionsGetRequest
from plaid.model.accounts_balance_get_request import AccountsBalanceGetRequest
from plaid.model.accounts_get_request import AccountsGetRequest
from plaid.model.investments_holdings_get_request import InvestmentsHoldingsGetRequest
from plaid.model.item_get_request import ItemGetRequest
from plaid.model.institutions_get_by_id_request import InstitutionsGetByIdRequest
from plaid.model.transfer_authorization_create_request import TransferAuthorizationCreateRequest
from plaid.model.transfer_create_request import TransferCreateRequest
from plaid.model.transfer_get_request import TransferGetRequest
from plaid.model.transfer_network import TransferNetwork
from plaid.model.transfer_type import TransferType
from plaid.model.transfer_user_in_request import TransferUserInRequest
from plaid.model.ach_class import ACHClass
from plaid.model.transfer_create_idempotency_key import TransferCreateIdempotencyKey
from plaid.model.transfer_user_address_in_request import TransferUserAddressInRequest
from plaid.api import plaid_api
from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify
from datetime import datetime
from datetime import timedelta
import plaid
import base64
import os
import datetime
import json
import time
from dotenv import load_dotenv
from werkzeug.wrappers import response
import pyodbc
import PublicKey
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import string
import random
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
#from flask_sslify import SSLify



load_dotenv()


app = Flask(__name__)
#sslify = SSLify(app)

# Fill in your Plaid API keys - https://dashboard.plaid.com/account/keys
PLAID_CLIENT_ID = os.getenv('PLAID_CLIENT_ID')
PLAID_SECRET = os.getenv('PLAID_SECRET')
# Use 'sandbox' to test with Plaid's Sandbox environment (username: user_good,
# password: pass_good)
# Use `development` to test with live users and credentials and `production`
# to go live
PLAID_ENV = os.getenv('PLAID_ENV', 'development')
# PLAID_PRODUCTS is a comma-separated list of products to use when initializing
# Link. Note that this list must contain 'assets' in order for the app to be
# able to create and retrieve asset reports.
PLAID_PRODUCTS = os.getenv('PLAID_PRODUCTS', 'transactions').split(',')

# PLAID_COUNTRY_CODES is a comma-separated list of countries for which users
# will be able to select institutions from.
PLAID_COUNTRY_CODES = os.getenv('PLAID_COUNTRY_CODES', 'US').split(',')

def getFernet():
	kdf = PBKDF2HMAC(
    	algorithm=hashes.SHA256(),
    	length=32,
    	salt=PublicKey.key,
    	iterations=390000,
	)
	key = base64.urlsafe_b64encode(kdf.derive(PublicKey.password))
	f = Fernet(key)
	return f

FernetKey = getFernet()

def encryptPass(password):
	bytePass = bytes(password, 'utf-8')
	token = FernetKey.encrypt(bytePass)
	return token

def decryptPass(FernetToken):
	bytePass = bytes(FernetToken, 'utf-8')
	return FernetKey.decrypt(bytePass).decode('utf-8')

def empty_to_none(field):
    value = os.getenv(field)
    if value is None or len(value) == 0:
        return None
    return value

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/webAPI/')
def webAPI():
  return render_template('webAPI.html')
  
@app.route('/createAccount')
def createAccount():
    return render_template('createAccount.html')

host = plaid.Environment.Sandbox

if PLAID_ENV == 'sandbox':
    host = plaid.Environment.Sandbox

if PLAID_ENV == 'development':
    host = plaid.Environment.Development

if PLAID_ENV == 'production':
    host = plaid.Environment.Production

# Parameters used for the OAuth redirect Link flow.
#
# Set PLAID_REDIRECT_URI to 'http://localhost:3000/'
# The OAuth redirect flow requires an endpoint on the developer's website
# that the bank website should redirect to. You will need to configure
# this redirect URI for your client ID through the Plaid developer dashboard
# at https://dashboard.plaid.com/team/api.
PLAID_REDIRECT_URI = empty_to_none('PLAID_REDIRECT_URI')

configuration = plaid.Configuration(
    host=host,
    api_key={
        'clientId': PLAID_CLIENT_ID,
        'secret': PLAID_SECRET,
        'plaidVersion': '2020-09-14'
    }
)

api_client = plaid.ApiClient(configuration)
client = plaid_api.PlaidApi(api_client)

products = []
for product in PLAID_PRODUCTS:
    print(Products(product))
    products.append(Products(product))


cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                      "Server=WIN-Q75VOTO0D0V\MSSQLSERVER_UPD;"
                      "Database=MyBudget;"
                      "Trusted_Connection=yes;"
                      "MARS_Connection=Yes")

@app.route('/api/info', methods=['POST'])
def info():
    global access_token
    global item_id
    return jsonify({
        'item_id': item_id,
        'access_token': access_token,
        'products': PLAID_PRODUCTS
    })

@app.route('/api/create_link_token', methods=['GET'])
def create_link_token():
    try:
        request = LinkTokenCreateRequest(
            products=products,
            client_name="Budgetary",
            country_codes=list(map(lambda x: CountryCode(x), PLAID_COUNTRY_CODES)),
            language='en',
            webhook='https://borr.live/api/webhook',
            user=LinkTokenCreateRequestUser(
                client_user_id=str(time.time())
            )
        )
        if PLAID_REDIRECT_URI!=None:
            request['redirect_uri']=PLAID_REDIRECT_URI
    # create link token
        response = client.link_token_create(request)
        print(response)
        return jsonify(response.to_dict())
    except plaid.ApiException as e:
        print(json.loads(e.body))
        return json.loads(e.body)


##remove an account
@app.route('/api/removeitem', methods=['POST'])
def remove_access_token():
    link_id = request.get_json(force=True)['link_id']
    cursor = cnxn.execute('{Call MyBudget.dbo.RemoveLink(?)}' , [link_id])
    rows = cursor.fetchall()
    accessToken = 0
    for row in rows:
        accessToken = row.API_ID
    if accessToken != 0:
        request = ItemRemoveRequest(access_token=accessToken)
        response = client.item_remove(request)

    data = {}
    data['RequestType'] = 'RemoveItem'
    cursor.commit()
    return jsonify(data) 


# Exchange token flow - exchange a Link public_token for
# an API access_token
# https://plaid.com/docs/#exchange-token-flow

@app.route('/api/set_access_token', methods=['POST'])
def get_access_token():
    public_token = request.get_json(force=True)['public_token']
    username = "mjbourquin"
    access_token = "" #error value
    try:
        exchange_request = ItemPublicTokenExchangeRequest(
            public_token=public_token)
        exchange_response = client.item_public_token_exchange(exchange_request)
        access_token = exchange_response['access_token']
        print(access_token);
        #item_id = exchange_response['item_id']
        ##if 'transfer' in PLAID_PRODUCTS:
        ##    transfer_id = authorize_and_create_transfer(access_token)
        ## print(exchange_response.to_dict())

        cursor = cnxn.execute('{Call MyBudget.dbo.SetAccessToken(?, ?)}' , [username,access_token])
        cursor.commit()

        data = {}
        data['value'] = 'Success'
        data['access_token'] = access_token

        return jsonify(data)
    except plaid.ApiException as e:
        cursor = cnxn.execute('{Call MyBudget.dbo.SetAccessToken(?, ?)}' , [username,access_token])
        cursor.commit()
        print(e.body)
        return json.loads(e.body)

def checkLoginBool(cursor):
	rows = cursor.fetchall()
	data = 0
	if len(rows) > 0:
		print(rows[0].Password)
		password = decryptPass(rows[0].Password)
		data = password
	
	return data

def CheckLogin(username, password):
	cursor = cnxn.execute("SELECT TOP 1 * FROM [User] WHERE Name = ?", [str(username)])
	data = checkLoginBool(cursor)

	result = 0
	if data == password:
		result = 1

	return result

# Retrieve Transactions for an Item
# https://plaid.com/docs/#transactions

@app.route('/api/getLatestLinkedAccount', methods=['GET'])
def getLatestLinkedAccount():
    cursor = cnxn.execute('{Call MyBudget.dbo.CheckLinkEvents(?)}' , [1])
    rows = cursor.fetchall()
    data = {}
    data['RequestType'] = 'LatestLinkedAccount'
    if len(rows) > 0:
        data['ValidAdd'] = rows[0].ValidAdd
        data['AccountId'] = rows[0].AccountID
        data['UserId'] = rows[0].UserId
        data['LinkEvent'] = 1
    else:
        data['ValidAdd'] = 0
        data['AccountId'] = 0
        data['UserId'] = 0
        data['LinkEvent'] = 0

    cursor.commit() 
    print(data)
    return jsonify(data)

@app.route('/updateCategory', methods=['POST'])
def updateCategoryDB():
    data = request.get_json(force=True)['Data']
    username = request.get_json(force=True)['username']
    Name = data['name']
    isActive = data['isActive']
    Show = data['Show']
    ParentId = data['parentId']
    ID = data['ID']

    print(username)
    print(data)

    cursor = cnxn.execute('{Call MyBudget.dbo.updateCategory(?,?,?,?,?,?)}' , [username, ID, Show,Name,isActive,ParentId])
    rows = cursor.fetchall()
    cursor.commit()
    data = {}
    data['RequestType'] = 'UpdateCategory'
    for row in rows:
        data['ID'] = row.Id
        data['name'] = row.Name
        data['parentId'] = row.ParentId
        data['isActive'] = row.State
        data['MasterCatId'] = row.Master_CatId
        data['Show'] = row.Show

    print(data)

    return jsonify(data)

@app.route('/updateAccount', methods=['POST'])
def updateAccountDB():
    data = request.get_json(force=True)['Data']
    username = request.get_json(force=True)['username']
    Name = data['name']
    isActive = data['isActive']
    ID = data['ID']

    print(username)
    print(data)

    cursor = cnxn.execute('{Call MyBudget.dbo.UpdateAccount(?,?,?,?)}' , [username, ID, isActive, Name])
    rows = cursor.fetchall()
    cursor.commit()
    data = {}
    data['RequestType'] = 'UpdateAccount'
    for row in rows:
        data['ID'] = row.Id
        data['name'] = row.Name
        data['balance'] = row.Amount
        data['isActive'] = row.State

    print(data)

    return jsonify(data)

@app.route('/updateBudget', methods=['POST'])
def updateBudgetDB():
    data = request.get_json(force=True)['Data']
    username = request.get_json(force=True)['username']
    Name = data['name']
    catId = data['catID']
    Amount = data['amount']
    isActive = data['isActive']
    ID = data['ID']

    print(username)
    print(data)

    cursor = cnxn.execute('{Call MyBudget.dbo.UpdateBudget(?,?,?,?,?,?)}' , [username, ID, isActive, Name, catId, Amount])
    rows = cursor.fetchall()
    cursor.commit()
    data = {}
    data['RequestType'] = 'UpdateBudget'
    for row in rows:
        data['ID'] = row.ID
        data['amount'] = row.Amount
        data['name'] = row.UniqueName
        data['isActive'] = row.State
        data['catId'] = row.CategoryID

    print(data)

    return jsonify(data)

@app.route('/updateMultiTransaction', methods=['POST'])
def updateMultiTransactionDB():
    data = request.get_json(force=True)['Data']
    username = request.get_json(force=True)['username']
    print(data)
    print(username)
    transIDs = []
    IDstring = ''
    for value in data:
        catId = value['catID']
        ID = value['ID']
        isActive = value['isActive']
        date = value['date']
        amount = value['amount']
        name = value['name']
        accountId = value['accountId']
        owner = value['owner']
        cursor = cnxn.execute('{Call MyBudget.dbo.UpdateTransaction(?, ?, ?, ?, ?, ?, ?, ?, ?)}' , [username, date, name, amount, catId, accountId, owner, ID, isActive])
        rows = cursor.fetchall()
        cursor.commit()
        for row in rows:
            transIDs.append(row.ID)

    list_len = len(transIDs)
    for ID in transIDs:
        current_idx = transIDs.index(ID)
        list_end = list_len - current_idx
        if(list_end == 1): ##at the end of the list
            IDstring = IDstring + str(ID)
        else:
            IDstring = IDstring + str(ID) + ','

    print(IDstring)

    cursor = cnxn.execute('{Call MyBudget.dbo.GetTransactions(?)}' , [IDstring])
    rows = cursor.fetchall()
    data = {}
    data['Data'] = []
    data['RequestType'] = 'UpdateMultiTransaction'
    for row in rows:
        value = {}
        value['ID'] = row.ID
        value['amount'] = row.amount
        value['name'] = row.name
        value['externalName'] = row.externalName
        value['date'] = row.date
        value['authdate'] = row.authdate
        value['isActive'] = row.isActive
        value['isPending'] = row.isPending
        value['ownership'] = row.ownership
        value['userId'] = row.userId
        value['catId'] = row.catId
        value['accountId'] = row.accountId

        data['Data'].append(value)

    print(data)
    return jsonify(data)

@app.route('/updateTransactionRule', methods=['POST'])
def updateTransactionRuleDB():
    data = request.get_json(force=True)['Data']
    username = request.get_json(force=True)['username']
    Name = data['Name']
    Type = data['Type']
    catId = data['CatID']
    NameTo = data['NameTo']

    print(username)
    print(data)

    cursor = cnxn.execute('{Call MyBudget.dbo.UpdateTransactionRule(?,?,?,?,?)}' , [username, Name, Type, catId, NameTo])
    cursor.commit()
    data = {}
    data['RequestType'] = 'UpdateTransactionRule'

    return jsonify(data)

@app.route('/updateTransaction', methods=['POST'])
def updateTransactionDB():
    data = request.get_json(force=True)['Data']
    username = request.get_json(force=True)['username']
    print(data)
    catId = data['catID']
    ID = data['ID']
    isActive = data['isActive']
    date = data['date']
    amount = data['amount']
    name = data['name']
    accountId = data['accountId']
    owner = data['owner']
    cursor = cnxn.execute('{Call MyBudget.dbo.UpdateTransaction(?, ?, ?, ?, ?, ?, ?, ?, ?)}' , [username, date, name, amount, catId, accountId, owner, ID, isActive])
    rows = cursor.fetchall()
    cursor.commit()
    data = {}
    data['RequestType'] = 'UpdateTransaction'
    for row in rows:
        data['ID'] = row.ID
        data['amount'] = row.amount
        data['name'] = row.name
        data['externalName'] = row.externalName
        data['date'] = row.date
        data['authdate'] = row.authdate
        data['isActive'] = row.isActive
        data['isPending'] = row.isPending
        data['ownership'] = row.ownership
        data['userId'] = row.userId
        data['catId'] = row.catId
        data['accountId'] = row.accountId

    print(data)

    return jsonify(data)


@app.route('/api/setLinkName', methods=['POST'])
def setLinkName():
    ID = request.get_json(force=True)['ID']
    name = request.get_json(force=True)['name']
    print(ID)
    print(name)
    cursor = cnxn.execute('{Call MyBudget.dbo.SetLinkName(?, ?)}' , [name,ID])
    cursor.commit();

    return jsonify(name)


@app.route('/login', methods=['GET','POST'])
def loginUser():
	username = request.get_json(force=True)['username']
	password = request.get_json(force=True)['widbkduqefl']
	password= base64.b64decode(password).decode('utf-8')
	data = {}
	data['check'] = 'Fail'
	data['username'] = username
	data['sessionID'] = ''
	if CheckLogin(username,password):
		data['check'] = 'Success'
		letters = string.ascii_letters
		sessionID = ''.join(random.choice(letters) for i in range(35))
		print("SessionID: " + sessionID)
		data['sessionID'] = sessionID
		cursor = cnxn.execute('{Call MyBudget.dbo.LoginUser(?, ?)}' , [sessionID,username])
		cursor.commit();

	print(data)
	return jsonify(data)

# Retrieve an Item's accounts
# https://plaid.com/docs/#accounts


@app.route('/PyisLoggedIn', methods=['GET','POST'])
def CheckIsUserLoggedIn():
	username = request.get_json(force=True)['username']
	sessionID = request.get_json(force=True)['sessionID']
	result = 0
	if sessionID != 'NULL':
		sessionID= base64.b64decode(sessionID).decode('utf-8')
		cursor = cnxn.execute('SELECT TOP 1 * FROM [User] WHERE Name = ? AND SessionID = ?', [username, sessionID])
		rows = cursor.fetchall()
		
		if len(rows) > 0:
			result = 1
		else:
			cursor = cnxn.execute('UPDATE [User] Set sessionID = NULL, ModifiedDate = GETUTCDATE(), ModifiedBy = -1 WHERE Name = ?', [username])
			cursor.commit()

	data = {}
	data['isLoggedIn'] = result
	data['username'] = username
	print(data)
	return jsonify(data)

@app.route('/getAccountsDB', methods=['GET','POST'])
def getAccountsDB():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.GetAccountInfo(?)}' , [username])
    rows = cursor.fetchall()

    data = {}
    data['RequestType'] = 'GetAccountInfo'
    data['Data'] = []

    for row in rows:
        rowdata = {}
        rowdata['AccountId'] = row.AccountId
        rowdata['UniqueName'] = row.UniqueName
        rowdata['AccountType'] = row.AccountType
        rowdata['Amount'] = row.Amount
        rowdata['HouseholdId'] = row.HouseholdId
        rowdata['HouseholdName'] = row.HouseholdName
        rowdata['UserOwnerId'] = row.UserOwnerId
        rowdata['UserOwnerName'] = row.UserOwnerName
        rowdata['PrivateUserId'] = row.PrivateUserId
        rowdata['PrivateName'] = row.PrivateName
        rowdata['ActViewable'] = row.ActViewable
        rowdata['ExternalName'] = row.ExternalName
        rowdata['FullExternalName'] = row.FullExternalName
        rowdata['ParentAccountID'] = row.ParentAccountID
        rowdata['isActive'] = row.isActive
        data['Data'].append(rowdata)

    return jsonify(data)

@app.route('/getTransactionsDB', methods=['GET','POST'])
def getTransactionsDB():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.GetTransactionsAll(?)}' , [username])
    rows = cursor.fetchall()
    data = {}
    data['RequestType'] = 'GetTransactionInfo'
    data['Data'] = []

    for row in rows:
        rowdata = {}
        rowdata['ID'] = row.ID
        rowdata['amount'] = row.amount
        rowdata['name'] = row.name
        rowdata['externalName'] = row.externalName
        rowdata['date'] = row.date
        rowdata['authdate'] = row.authdate
        rowdata['isActive'] = row.isActive
        rowdata['isPending'] = row.isPending
        rowdata['ownership'] = row.ownership
        rowdata['userId'] = row.userId
        rowdata['catId'] = row.catId
        rowdata['accountId'] = row.accountId
        data['Data'].append(rowdata)

    return jsonify(data)

@app.route('/getBudgetsDB', methods=['GET','POST'])
def getBudgetsDB():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.GetBudgetInfo(?)}' , [username])
    rows = cursor.fetchall()
    data = {}
    data['RequestType'] = 'GetBudgetInfo'
    data['Data'] = []

    for row in rows:
        rowdata = {}
        rowdata['ID'] = row.ID
        rowdata['categoryId'] = row.categoryId
        rowdata['name'] = row.name
        rowdata['type'] = row.type
        rowdata['amount'] = row.amount
        rowdata['startingDay'] = row.startingDay
        rowdata['isActive'] = row.isActive
        data['Data'].append(rowdata)

    return jsonify(data)

@app.route('/getCategoriesDB', methods=['GET','POST'])
def getCategoriesDB():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.GetCategoryInfo(?)}' , [username])
    rows = cursor.fetchall()
    data = {}
    data['RequestType'] = 'GetCategoryInfo'
    data['Data'] = []

    for row in rows:
        rowdata = {}
        rowdata['ID'] = row.ID
        rowdata['parentId'] = row.parentId
        rowdata['name'] = row.name
        rowdata['isActive'] = row.isActive
        rowdata['isShown'] = row.isShown
        rowdata['MasterId'] = row.MasterId
        data['Data'].append(rowdata)

    print(data);

    return jsonify(data)

@app.route('/getLinkedBanksDB', methods=['GET','POST'])
def getLinkedBanksDB():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.GetLinkedBankInfo(?)}' , [username])
    rows = cursor.fetchall()
    data = {}
    data['RequestType'] = 'GetLinkedBankInfo'
    data['Data'] = []
    for row in rows:
        rowdata = {}
        rowdata['AccountId'] = row.LinkedActID
        rowdata['LinkedName'] = row.LinkedName
        rowdata['isActive'] = row.isActive
        data['Data'].append(rowdata)

    return jsonify(data)

@app.route('/api/webhook', methods=['POST'])
def processWebHook():
    data = request.data
    cursor = cnxn.execute('INSERT INTO webhookResults (webhookJSON) VALUES (?)', [data])
    cursor.commit()
    print(data)
    return 'NULL'

# Retrieve high-level information about an Item
# https://plaid.com/docs/#retrieve-item


@app.route('/api/item', methods=['GET'])
def item():
    try:
        request = ItemGetRequest(access_token=access_token)
        response = client.item_get(request)
        request = InstitutionsGetByIdRequest(
            institution_id=response['item']['institution_id'],
            country_codes=list(map(lambda x: CountryCode(x), PLAID_COUNTRY_CODES))
        )
        institution_response = client.institutions_get_by_id(request)
        pretty_print_response(response.to_dict())
        pretty_print_response(institution_response.to_dict())
        return jsonify({'error': None, 'item': response.to_dict()[
            'item'], 'institution': institution_response.to_dict()['institution']})
    except plaid.ApiException as e:
        error_response = format_error(e)
        return jsonify(error_response)

def pretty_print_response(response):
  print(json.dumps(response, indent=2, sort_keys=True, default=str))

def format_error(e):
    response = json.loads(e.body)
    return {'error': {'status_code': e.status, 'display_message':
                      response['error_message'], 'error_code': response['error_code'], 'error_type': response['error_type']}}


def updateCategoriesAPI():
    response = client.categories_get({})
    categories = response['categories']
    data = {}
    data['RequestType'] = 'UpdateMasterCategories'
    data['Data'] = []
    for category in categories:
        hierarchy = category['hierarchy']
        catid = category['category_id']
        name = ', '.join(hierarchy)
        parentname = ''
        count = 0
        if len(hierarchy) > 1:
            for cat in hierarchy:
                if count <= len(hierarchy) - 2:
                    if count == 0:
                        parentname = parentname + cat
                    else:
                        parentname = parentname + ', ' + cat
                else:
                    break
                count = count + 1
        else:
            parentname = 'NULL'

        catsend = {}
        catsend['catID'] = catid
        catsend['name'] = name
        catsend['parentname'] = parentname
        data['Data'].append(catsend)

        cursor = cnxn.execute('{Call MyBudget.dbo.UpdateAPICategories(?, ?, ?)}' , [catid, name, parentname])
        cursor.commit();

    return data

@app.route('/api/updateCategories', methods=['GET'])
def updateCategories():
    
    data = updateCategoriesAPI()

    return jsonify(data)

def updateAccounts():
    username = 'mjbourquin'
    #print(request.get_json(force=True)['username'])
    cursor = cnxn.execute('{Call MyBudget.dbo.GetAPIParentAccountInfo(?)}' , [username])
    rows = cursor.fetchall()
    for row in rows:
        access_token = row.API_ID
        try:
            request1 = AccountsGetRequest(
                access_token=access_token
            )
            response = client.accounts_get(request1)
            accounts = response.to_dict()['accounts']
            #print(accounts)
            for account in accounts:
                account_id = account['account_id']
                unique_name = account['mask']
                name = account['name']
                account_type = account['type']
                cash =  account['balances']['current']
                user_accountname = 'NULL'
                cursor = cnxn.execute('{Call MyBudget.dbo.UpdateUserAccounts(?, ?, ?, ?, ?, ?, ?)}' , [account_id, unique_name,name,account_type,cash, access_token,user_accountname])
                cursor.commit() 
        except plaid.ApiException as e:
            error_response = format_error(e)
            print("Access Token: " + str(access_token) + ", Error: " + str(error_response))

@app.route('/api/accounts', methods=['GET'])
def get_accountsPLAID():
    
    updateAccounts()

    data = {}
    data['RequestType'] = 'UpdateAccounts'
    return jsonify(data)

def updateTransactions():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.GetAPIParentAccountInfo(?)}' , [username])
    rows = cursor.fetchall()
    end_date = datetime.datetime.now()
    #print(end_date)
    for row in rows:
        access_token = row.API_ID
        start_date = row.LastUpdated - timedelta(days=14)
        #start_date = (datetime.datetime.now() - timedelta(days=90))
        print("Account Last Updated: " + str(start_date))
        print("End Date: " + str(end_date))
        
        #print(start_date)
        

        try:
            options = TransactionsGetRequestOptions()
            request1 = TransactionsGetRequest(
                access_token=access_token,
                start_date=start_date.date(),
                end_date=end_date.date(),
                options=options
            )
            response = client.transactions_get(request1)
            transactions = response.to_dict()['transactions']
            #transactioncnt = 0
            #print(accounts)
            for transaction in transactions:
                account_id = transaction['account_id']
                amount = transaction['amount']
                currencycode = transaction['iso_currency_code']
                category_id = transaction['category_id']
                date = str(transaction['date'])
                authorized_date = str(transaction['authorized_date'])
                category_id = transaction['category_id']
                name = transaction['name']
                merchant_name = transaction['merchant_name']
                pending = transaction['pending']
                transaction_type = transaction['transaction_type']
                transaction_id = transaction['transaction_id']
                location = str(transaction['location'])
                ##print("\n")
                ##print(transaction)
                cursor = cnxn.execute('{Call MyBudget.dbo.UpdateAccountTransactions(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}' , [account_id, amount,currencycode,category_id,date, authorized_date,name,merchant_name, pending, transaction_type, transaction_id, username, location])
                cursor.commit();
                   #print(f"Call MyBudget.dbo.UpdateAccountTransactions {account_id}, {amount},{currencycode},{category_id},{datetime1}, {authorized_datetime}, {name}, {merchant_name}, {pending},    {transaction_type}, {transaction_id}, {username}")
                   #transactioncnt = accountcnt + 1
        except plaid.ApiException as e:
            error_response = format_error(e)
            print(error_response)


@app.route('/api/transactions', methods=['GET'])
def get_transactionsPLAID():
    # Pull transactions since the last user login
    
    updateTransactions()
    
    data = {}
    data['RequestType'] = 'UpdateTransactions'
    return jsonify(data)

def remove_OldPLAIDFunc():
    username = 'mjbourquin'
    cursor = cnxn.execute('{Call MyBudget.dbo.RemoveOldProc(?)}' , [username])
    cursor.commit()

@app.route('/api/removeold', methods=['GET'])
def remove_oldPLAID():

    remove_OldPLAIDFunc()

    data = {}
    data['RequestType'] = 'RemoveOld'
    return jsonify(data)

scheduler = BackgroundScheduler()
scheduler.add_job(func=updateCategoriesAPI, trigger="interval", days=1)
scheduler.add_job(func=updateAccounts, trigger="interval", days=1)
scheduler.add_job(func=updateTransactions, trigger="interval", days=1)
scheduler.add_job(func=remove_OldPLAIDFunc, trigger="interval", days=1)
scheduler.start()

atexit.register(lambda: scheduler.shutdown())

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.getenv('PORT', 80))
