//check login

var public_tokenvar;

///////////////////////////////////////////////
      function GetisLoggedIn()
      {
        loginInfo = sessionStorage.getItem("logininfo");
        var returnLoggedIn = false;
        document.getElementById('isLoggedIn').innerHTML = 'Check Log In';
        document.getElementById('loginoutput').innerHTML = sessionStorage.getItem("logininfo");
        $.ajax({
            type: "POST",
            url: "/PyisLoggedIn",
            data: loginInfo,
            encode: true,
            datatype: JSON,
            async: false,
            success: function(data){
              isLoggedIn = data['isLoggedIn']
              username = data['username']
              if(isLoggedIn)
              {
                returnLoggedIn = true;
                document.getElementById('isLoggedIn').innerHTML = 'Logged In';
                hideDivElement('loginform'); //logged in, hide the login form
                document.getElementById('loginoutput').innerHTML = sessionStorage.getItem("logininfo");
              }
              else
              {
                document.getElementById('isLoggedIn').innerHTML = 'Logged Out';
                json = '{"username" : "' + username + '","sessionID":"' + 'NULL' + '"}';
                sessionStorage.setItem("logininfo", json);
                showDivElement('loginform'); //logged in, hide the login form
                document.getElementById('loginoutput').innerHTML = sessionStorage.getItem("logininfo");
              }
            }
        });
        return returnLoggedIn;
      }

      function GetDBAccounts()
      {
        if(GetisLoggedIn())
        {
          loginInfo = sessionStorage.getItem("logininfo");
          jsonloginInfo = JSON.parse(loginInfo);
          username = jsonloginInfo.username;
		  //document.getElementById('accounts').innerHTML = username;
          var formData = {
            username: username
          };
          $.ajax({
             type: "POST",
             url: "/getAccounts",
             data: JSON.stringify(formData),
             encode: true,
             success: function(data){
					  var accounts = document.createElement("div");
					  accounts.id = "accounts";
					  
					  //jsondata = JSON.parse(data);
					  /*
					  rowdata['AccountId'] = row.AccountId
					  rowdata['UniqueName'] = row.UniqueName
        rowdata['AccountType'] = row.AccountType
        rowdata['Amount'] = row.Amount
        rowdata['HouseholdId'] = row.HouseholdId
        rowdata['HouseholdName'] = row.HouseholdName
        rowdata['UserOwnerId'] = row.UserOwnerId
        rowdata['UserOwnerName'] = row.UserOwnerName
        rowdata['PrivateUserId'] = row.PrivateUserId
        rowdata['PrivateName'] = row.PrivateName
        rowdata['ActViewable'] = row.ActViewable
        rowdata['ExternalName'] = row.ExternalName
        rowdata['FullExternalName'] = row.FullExternalName*/
					  for (key in data)
					  {
						var AccountDiv = document.createElement("div");
						var Accountbutton = document.createElement("button");
						Accountbutton.innerHTML = "Edit";
						var AccountName = data[key]['UniqueName'];
						if(AccountName == "Null")
						{
							AccountName = data[key]['FullExternalName'];
						}
						AccountDiv.id = data[key]['AccountId'];
						var AccountBody = document.createElement("div");
						AccountBody.innerHTML = 'Account Name:' + AccountName + ', Balance: ' + data[key]['Amount'];
						AccountDiv.appendChild(AccountBody);
						AccountDiv.appendChild(Accountbutton);
						accounts.appendChild(AccountDiv);
					  }
					  //create accounts list
					  document.getElementById("AccountsBody").innerHTML = "";
					  document.getElementById("AccountsBody").appendChild(accounts);
                  },
              });
          }
      }

      function Logout()
      {
          json = '{"username" : "' + username + '","sessionID":"' + 'NULL' + '"}';
          sessionStorage.setItem("logininfo", json);
          showDivElement('loginform'); //logged in, hide the login form
          document.getElementById('loginoutput').innerHTML = sessionStorage.getItem("logininfo");
          document.getElementById('isLoggedIn').innerHTML = 'Logged Out';
      }


    	function submitLogin()
    	{
        if(!GetisLoggedIn())
        {
          var inputUserName = document.getElementById("myUsername").value;
          var inputwidbkduqefl = document.getElementById("mywidbkduqefl").value;
          var formData = {
            username: inputUserName,
            widbkduqefl: btoa(inputwidbkduqefl),
          };
          //document.getElementById('loginoutput').innerHTML = JSON.stringify(formData);
          $.ajax({
             type: "POST",
             url: "/login",
             data: JSON.stringify(formData),
             encode: true,
             success: function(data){
                      //document.getElementById('loginoutput').innerHTML = JSON.stringify(data);
                      if(data['check'] == 'Success')
                      {
                        username = data['username'];
                        sessionID = btoa(data['sessionID'])
                        json = '{"username" : "' + username + '","sessionID":"' + sessionID + '"}';
                        sessionStorage.setItem("logininfo", json);
                        hideDivElement('loginform'); //logged in, hide the login form
                        document.getElementById('isLoggedIn').innerHTML = 'Logged In';
                      }
                      else { document.getElementById('isLoggedIn').innerHTML = 'Logged Out'; 
                        json = '{"username" : "' + 'NULL' + '","sessionID":"' + 'NULL' + '"}';
                        sessionStorage.setItem("logininfo", json);
                        document.getElementById('loginoutput').innerHTML = json;
                    }
                  },
              });
        }
        else
        {
          //already logged in
        }
    	}

      function hideDivElement(id)
      {
        var x = document.getElementById(id);
        x.style.display = "none";
      }

      function showDivElement(id)
      {
        var x = document.getElementById(id);
        x.style.display = "block";
      }

    	function UpdateAPICategories(){
            $.ajax({
                type: "POST",
                url: "/api/updateCategories",
                success: function(data){
                    
                }
            });
        }

        function GetPublicPlaid(){
            $.ajax({
                type: "GET",
                url: "/api/create_link_token",
                datatype: JSON,
                success: function(data){
                    //document.getElementById('plaidToken').innerHTML = JSON.stringify(data);
                    launchPlaid(data["link_token"]);
                }
            });
        }
        function launchPlaid(data) {
            const handler = Plaid.create({
                token: data,
                onSuccess: (public_token, metadata) => {
					public_tokenvar = public_token;
				  ChooseNameForAccount();
                  //SetAccessTokenPlaid(public_token);
                },
                onLoad: () => { },
                            onExit: (err, metadata) => { 
                  //document.getElementById('plaidToken').innerHTML = err;
                  //document.getElementById('plaidMetadata2').value = JSON.stringify(metadata, null, 2); 
                },
                onEvent: (eventName, metadata) => { },
                receivedRedirectUri: null,
            });
                // Calling open() will display the "Institution Select" view to your user, 
                // starting the Link flow. 
            handler.open();
        }
		
		function ChooseAccountType()
		{
			var optionList = document.getElementById("AccountTypeSelect");
			var selectedtext = optionList.options[optionList.selectedIndex].text;
			console.log(selectedtext);
			if(selectedtext == "Manual")
			{
				
			}
			else if(selectedtext == "Existing")
			{
				parent.GetPublicPlaid();
			}
		}

        function SetAccessTokenPlaid(parentAccountName)
		{
            if(GetisLoggedIn())
            {
				loginInfo = sessionStorage.getItem("logininfo");
				jsonloginInfo = JSON.parse(loginInfo);
				username = jsonloginInfo.username;
				var formData = 
				{
					username: username,
					public_token: public_tokenvar,
					parent_account_name: parentAccountName
				};
            
			
				$.ajax
				({
					type: "POST",
					url: '/api/set_access_token',
					data: JSON.stringify(formData),
					datatype: JSON,
					success: function(data)
					{
						//get the accountid from the data
						if(data['value'] == 'Success')
						{
							//successfully added an account -> guide the user
							endCreateParentAccount(data['parent_account_name']);
						}
					},
				});
			}
        }

        function UpdateTransactionsPlaid(){
            if(GetisLoggedIn())
            {
              loginInfo = sessionStorage.getItem("logininfo");
            jsonloginInfo = JSON.parse(loginInfo);
            username = jsonloginInfo.username;
            var formData = {
              username: username
            };
            $.ajax({
                type: "POST",
                url: "/api/transactions",
                data: JSON.stringify(formData),
                datatype: JSON,
                success: function(data){
                    document.getElementById('transactions').innerHTML = JSON.stringify(data);
                },
            });
            }
        }

        function UpdateAccountsPlaid(){
          if(GetisLoggedIn())
          {
            loginInfo = sessionStorage.getItem("logininfo");
            jsonloginInfo = JSON.parse(loginInfo);
            username = jsonloginInfo.username;
            var formData = {
              username: username
            };
            $.ajax({
                type: "POST",
                url: "/api/accounts",
                data: JSON.stringify(formData),
                datatype: JSON,
                success: function(data){
                    document.getElementById('accounts').innerHTML = JSON.stringify(data);
                },
            });
          }
        }
		

function CPChooseType(event)
{
	var optionList = document.getElementById("ChooseParentTypeSelect");
	var selectedtext = optionList.options[optionList.selectedIndex].text;
	console.log(selectedtext);
	if(selectedtext == "Manual")
	{
				
	}
	else if(selectedtext == "Existing")
	{
		GetPublicPlaid();
	}
}

function endCreateParentAccount(parentAccountName)
{
	CreateParentEl = document.getElementById("CreateParentEl");
	CreateParentEl.innerHTML = "";
	
	text1 = document.createElement('div');
	text1.innerHTML = "Successfully Created Account: " + parentAccountName +". Close Window to Continue";
	
	submitDiv = document.createElement('div');
		//submitDiv.className = "item5";
		var closeImage = new Image();
		closeImage.src = "https://cdn2.iconfinder.com/data/icons/media-controls-5/100/close-512.png";
		closeImage.height = "50";
		closeImage.width = "50";
		closeImage.title = "Close";
		submitDiv.appendChild(closeImage);
		submitDiv.addEventListener("click", function(){ 
			remEl = document.getElementById("CreateParentEl");
			
			document.body.removeChild(remEl); //remove the create Account Window Div
		});
		
	public_tokenvar = 'NULL';
		
	CreateParentEl.appendChild(text1);
	CreateParentEl.appendChild(subDiv);
}

function ChooseNameForAccount()
{
	CreateParentEl = document.getElementById("CreateParentEl");
	CreateParentEl.innerHTML = "";
	
	accountNameDiv = document.createElement("div");
	accountNameText = "Choose Account Name: <br>";
	accountNameDiv.innerHTML = accountNameText;
	
	textbox = document.createElement('input'); 
	input.type = "text";
	textbox.id = "ChoosePA_NameText";
	
	
	submitDiv = document.createElement('div');
		//submitDiv.className = "item5";
		var closeImage = new Image();
		closeImage.src = "https://previews.123rf.com/images/sarahdesign/sarahdesign1403/sarahdesign140301003/26699893-submit-icon.jpg";
		closeImage.height = "50";
		closeImage.width = "50";
		closeImage.title = "Submit";
		submitDiv.appendChild(closeImage);
		submitDiv.addEventListener("click", CPChoosePA_Name);
		
	CreateParentEl.appendChild(accountNameDiv);
	CreateParentEl.appendChild(textbox);
	CreateParentEl.appendChild(submitDiv);
	
	errorResponse = document.createElement('div');
	errorResponse.id = "CreateParentAccountError";
	CreateParentEl.appendChild(errorResponse);
}

function CPChoosePA_Name()
{
	if(public_tokenvar != null)
	{
		var accountName = document.getElementById("ChoosePA_NameText");
		if(accountName == ""){ document.getElementById("CreateParentAccountError").innerHTML("Error. Please Enter a Name");
		SetAccessTokenPlaid(accountName);
	}
}

function createAccount()
{
	if(GetisLoggedIn())
	{
		/*
		popUpCP = document.createElement('div');
		popUpCP.className = "popupmenu";
		popUpCP.Id = "CreateParentDiv";
		*/
		
		createParentEl = document.createElement('div');
		createParentEl.className = 'iframe';
		createParentEl.Id = "CreateParentEl";
		//popUpCP.appendChild(createParentEl);
		
		topleftdiv = document.createElement('div');
		//topleftdiv.className = "leftitem";
		
		/*
		var closeImage = new Image();
		closeImage.src = "https://cdn2.iconfinder.com/data/icons/media-controls-5/100/close-512.png";
		closeImage.height = "10";
		closeImage.width = "10";
		closeImage.title = "Close";
	https://previews.123rf.com/images/sarahdesign/sarahdesign1403/sarahdesign140301003/26699893-submit-icon.jpg
		
		
		topleftdiv.appendChild(closeImage);
		*/
		
		toprightdiv = document.createElement('div');
		//toprightdiv.className = "rightitem";
		toprightdiv.innerHTML = "Create Account";
		
		mainleftdiv = document.createElement('div');
		//mainleftdiv.className = "mainleft";
		
		selectBox = document.createElement('select');
		selectBox.Id = "ChooseParentTypeSelect";
		option1 = document.createElement("option");
		option1.text = "Manual";
		option1.value = "Manual";
		option2 = document.createElement("option");
		option2.text = "Link Existing";
		option1.value = "Manual";
		
		selectBox.appendChild(option1);
		selectBox.appendChild(option2);
		
		mainleftdiv.appendChild(selectBox);
		
		submitDiv = document.createElement('div');
		//submitDiv.className = "item5";
		var closeImage = new Image();
		closeImage.src = "https://previews.123rf.com/images/sarahdesign/sarahdesign1403/sarahdesign140301003/26699893-submit-icon.jpg";
		closeImage.height = "50";
		closeImage.width = "50";
		closeImage.title = "Close";
		submitDiv.appendChild(closeImage);
		submitDiv.addEventListener("click", CPChooseType);
		
		topleftdiv.Id = "CPtopleftdiv";
		toprightdiv.Id = "CPtoprightdiv";
		mainleftdiv.Id = "CPmainleftdiv";
		submitDiv.Id = "CPfooterdiv"
		
		createParentEl.appendChild(topleftdiv);
		createParentEl.appendChild(toprightdiv);
		createParentEl.appendChild(mainleftdiv);
		createParentEl.appendChild(submitDiv);
		
		document.body.appendChild(createParentEl);
		
		/*
		var iframe = document.getElementById("Create Account");
		iframe.style.display = 'block';
		iframe.contentWindow.document.getElementById('ChooseAccountTypeDiv').style.display = 'block';
		*/
	}
}

function CloseCreateAccountFrame()
{
	document.getElementById("Create Account").style.display = 'none';
}