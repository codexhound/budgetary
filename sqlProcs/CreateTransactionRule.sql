USE [MyBudget]
GO
/****** Object:  StoredProcedure [dbo].[CreateTransactionRule]    Script Date: 4/13/2022 8:07:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CreateTransactionRule]
(
	@username varchar(50),
	@lookupValue varchar(100),
	@lookupType int,
	@catid int,
	@type int,
	@ownership varchar(50),
	@renamevalue varchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @householdid int = (SELECT TOP 1 HouseholdId FROM [User] WHERE name = @username)
	
	DECLARE @userid int  = (SELECT TOP 1 Id FROM [User] u WHERE u.Name = @username);
	DECLARE @specific_user int = (SELECT TOP 1 Id FROM [User] u WHERE u.Name = @ownership AND @ownership != -1 AND @ownership != 0)

	DECLARE @owner int = (SELECT CASE WHEN @specific_user IS NOT NULL THEN @specific_user ELSE @ownership END AS owner FROM [User] WHERE Id = @userid)

	-- interfering with SELECT statements.
	IF NOT EXISTS (SELECT 1 FROM Rules r
	WHERE r.HouseholdId = @householdid AND LookupValue = @lookupValue AND (r.CategoryId = @catid AND @catid IS NOT NULL) OR (Ownership = @owner AND @owner IS NOT NULL) OR (RenameValue = @renamevalue OR @renamevalue IS NOT NULL)
	)
	BEGIN
		INSERT INTO Rules (LookupValue, HouseholdId, [Ownership], CategoryId, RenameValue, UserId, LookupValueType)
		VALUES (@lookupValue, @householdid, @owner, @catid, @renamevalue, @userid, @lookupType)
	END
	/*
	credit: Credit card
depository: Depository account
loan: Loan account
other: Non-specified account type
*/
    -- Insert statements for procedure here
	
END
