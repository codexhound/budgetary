USE [MyBudget]
GO
/****** Object:  StoredProcedure [dbo].[UpdateAccountTransactions]    Script Date: 4/13/2022 8:07:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[UpdateAccountTransactions]
(
    @account_id varchar(255),
	@amount float,
	@currencycode varchar(50),
	@category_id varchar(50),
	@datetime datetime,
	@authorized_datetime datetime,
	@name varchar(60),
	@merchant_name varchar(60),
	@pending int,
	@transaction_type varchar(50),
	@transaction_id varchar(255),
	@username varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP 1 * INTO #AccountTemp FROM Accounts WHERE API_ID = @account_id

	DECLARE @owner int = (SELECT OwnerId FROM #AccountTemp)
	DECLARE @ownerid int = (SELECT UserId FROM #AccountTemp)
	DECLARE @householdid int = (SELECT u.HouseholdId FROM [User] u WHERE name = @username)

	SELECT * INTO #RuleTemp FROM (
	SELECT TOP 1 * FROM (
	SELECT * FROM Rules WHERE HouseholdId = @householdid AND LookupValueType = 0 AND @name LIKE '%' + LookupValue + '%'
	UNION
	SELECT * FROM Rules WHERE HouseholdId = @householdid AND LookupValueType = 1 AND @name LIKE LookupValue + '%'
	UNION
	SELECT * FROM Rules WHERE HouseholdId = @householdid AND LookupValueType = 2 AND @name = LookupValue
	UNION
	SELECT * FROM Rules WHERE HouseholdId = @householdid AND LookupValueType = 3 AND @name LIKE '%' + LookupValue) AS V ORDER BY Id DESC) AS W

	DECLARE @defaultcatid int = (SELECT TOP 1 DefaultCatId FROM Households WHERE Id = @householdid)

	DECLARE @renameValue varchar(60) = (SELECT RenameValue FROM #RuleTemp)

	DECLARE @private_user int = (SELECT UserId FROM [#RuleTemp])
	DECLARE @ruleowner int = (SELECT Ownership FROM [#RuleTemp])

		IF (@owner = -1 OR @owner = 0 OR @ownerid = @private_user) AND @ruleowner IS NOT NULL --can't override the owner if the account is owned by a specific user (or the override is the user the account is owned by
		BEGIN
			SET @owner = @ruleowner
			IF @owner = 0 --private
			BEGIN
				SET @ownerid = @private_user
			END
		END

	IF @renameValue IS NOT NULL
	BEGIN
		SET @name = @renameValue
	END

	DECLARE @catid int = (SELECT CategoryId FROM #RuleTemp)
	IF @catid IS NULL
	BEGIN
		SET @catid = (SELECT TOP 1 c.Id FROM DefaultCategory dc
		INNER JOIN Category c on c.Master_CatId = dc.Id
		INNER JOIN [User] u on u.Name = @name AND u.[State] = 100
		INNER JOIN [Households] hh on hh.Id = u.HouseholdId
		WHERE (dc.API_ID = @category_id OR dc.API_ID2 = @category_id))
	END

	IF @catid IS NULL
	BEGIN
		SET @catid = @defaultcatid
	END

	UPDATE Transactions Set Owner = @owner, OwnerId = @ownerid, Name = @name, Merchant_Name = @merchant_name, Pending = @pending, Date = @datetime, ModifedBy = -1, [Type] = 0, ModifiedDate = GETUTCDATE() WHERE API_ID = @transaction_id

	INSERT INTO Transactions (HouseholdId, Name, Merchant_Name, Type, Pending, Owner, OwnerId, Date, Auth_Date, API_ID, CatId, CurrencyCode) 
	SELECT @householdid, @name, @merchant_name, 0, @pending, @owner, @ownerid, @datetime, @authorized_datetime, @transaction_id, @catid, @currencycode FROM Transactions 
	WHERE NOT EXISTS (SELECT 1 FROM Transactions WHERE API_ID = @transaction_id)

	DROP TABLE #RuleTemp
	DROP TABLE #AccountTemp
	/*
cnxn.execute('{Call MyBudget.dbo.UpdateAccountTransactions(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}' , [account_id, amount,currencycode,category_id,datetime, authorized_datetime, name, merchant_name, pending, transaction_type, transaction_id, username])
*/

	/*
	credit: Credit card
depository: Depository account
loan: Loan account
other: Non-specified account type
*/
    -- Insert statements for procedure here
	
END


