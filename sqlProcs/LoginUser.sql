USE [MyBudget]
GO
/****** Object:  StoredProcedure [dbo].[LoginUser]    Script Date: 4/13/2022 8:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[LoginUser]
(
    @session_ID NVARCHAR(1024)
    , @username NVARCHAR(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [User] Set ModifiedBy = -1, ModifiedDate = GETUTCDATE(), SessionId = @session_ID WHERE Name = @username

	/*
	credit: Credit card
depository: Depository account
loan: Loan account
other: Non-specified account type
*/
    -- Insert statements for procedure here
	
END
