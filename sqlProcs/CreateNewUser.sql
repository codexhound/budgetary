USE [MyBudget]
GO
/****** Object:  StoredProcedure [dbo].[CreateNewUser]    Script Date: 4/13/2022 8:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[CreateNewUser]
(
    @username varchar(50),
	@password varchar(255)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Create User
	--Create Household
	--Create Categories
	DECLARE @HouseHoldId int = (SELECT HouseholdId FROM [User] u WHERE name = @username)

	INSERT INTO Category (Name, HouseholdId, Master_CatId)
	SELECT dc.Name, @HouseHoldId, dc.Id FROM DefaultCategory dc
	--INNER JOIN Category c on c.Master_CatId = dc.Id
	WHERE dc.ParentId IS NULL

	INSERT INTO Category (ParentId, Name, HouseholdId, Master_CatId)
	SELECT pc.Id AS ParentId, dc.Name AS Name, @HouseHoldId AS HouseholdId, dc.Id AS Master_CatId 
	FROM DefaultCategory dc
	INNER JOIN DefaultCategory dcp on dcp.Id = dc.ParentId
	INNER JOIN Category pc on pc.Master_CatId = dcp.Id
	WHERE dc.ParentId IS NOT NULL
	/*
cnxn.execute('{Call MyBudget.dbo.UpdateAccountTransactions(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}' , [account_id, amount,currencycode,category_id,datetime, authorized_datetime, name, merchant_name, pending, transaction_type, transaction_id, username])
*/

	/*
	credit: Credit card
depository: Depository account
loan: Loan account
other: Non-specified account type
*/
    -- Insert statements for procedure here
	
END


