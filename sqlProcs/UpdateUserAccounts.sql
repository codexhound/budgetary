USE [MyBudget]
GO
/****** Object:  StoredProcedure [dbo].[UpdateUserAccounts]    Script Date: 4/13/2022 8:07:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[UpdateUserAccounts]
(
    @account_ID NVARCHAR(1024)
    , @unique_name NVARCHAR(100)
    , @name NVARCHAR(100)
	, @account_type NVARCHAR(100)
	, @cash float
	, @access_token NVARCHAR(1024)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM ParentAct;
	SELECT * FROM Accounts;

	DECLARE @int_type int = 0;
	SET @int_type = (SELECT CASE WHEN @account_type = 'Credit card' THEN 0 WHEN @account_type = 'Depository account' THEN 1 WHEN @account_type = 'Loan account' THEN 2 ELSE 3 END AS a)

	INSERT INTO Accounts (FullExternalName, [ExternalName],[Type], Amount, HouseholdId, OwnerId, ParentID, API_ID)
	SELECT @name, @unique_name, @int_type,@cash, u.HouseholdId, pa.UserId, pa.Id, @account_ID FROM ParentAct pa
	INNER JOIN [User] u on u.Id = pa.UserId
	WHERE NOT EXISTS (SELECT 1 FROM accounts WHERE API_ID = @account_ID) AND pa.API_ID = @access_token

	UPDATE Accounts Set FullExternalName = @name, [ExternalName] = @unique_name, [Type] = @int_type, Amount = @cash, HouseholdId = 1, ModifiedDate = GETUTCDATE(), ModifiedBy = -1
	WHERE API_ID = @account_ID

	/*
	credit: Credit card
depository: Depository account
loan: Loan account
other: Non-specified account type
*/
    -- Insert statements for procedure here
	
END
