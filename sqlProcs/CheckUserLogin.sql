USE [MyBudget]
GO
/****** Object:  StoredProcedure [dbo].[CheckUserLogin]    Script Date: 4/13/2022 8:06:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CheckUserLogin]
(
    @session_ID NVARCHAR(1024)
    , @username NVARCHAR(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	UPDATE [User] Set ModifiedBy = -1, ModifiedDate = GETUTCDATE(), SessionId = NULL WHERE SessionID IS NOT NULL AND ModifiedDate < GETDATE() - 1
	/*
	credit: Credit card
depository: Depository account
loan: Loan account
other: Non-specified account type
*/
    -- Insert statements for procedure here
	
END
